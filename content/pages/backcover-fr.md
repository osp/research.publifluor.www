Title: Backcover
Date: 2024-12-03 10:20 
Authors: Olivier Bertrand 
Summary: résumé  
Slug: backcover
Lang: fr


Autodidacte, Chrystel Crickx découpait à la main et vendait à la pièce
des lettres dans son magasin Publi Fluor à Schaerbeek. Découpées dans du
vinyle adhésif entre 1975 et 2000 pour des usages signalétiques et
publicitaires locaux, ces lettres ont depuis été numérisées et rendues
plus largement accessibles à des utilisateur·ices en dehors des
frontières belges et dans d'autres contextes.

À la marge des moyens de communication normés, elles ont contribué et
contribuent toujours à l'environnement visuel urbain, à Bruxelles et
ailleurs. Cet essai collectif non standard tente à la fois de raconter
la vie d'un modèle de lettres — avec celles de ses auteur·ices
successif·ves et leurs outils — tout en élargissant le champ pour
suivre les lézardes entre les différentes histoires que la pratique de Chrystel Crickx convoque.


- 384 pages, 16,8 × 23 cm
- FR/NL/EN
- &#127799; 7 couleurs, couverture souple, reliure suisse
- ISBN 978-2-931110-10-2
- 37€
- &#9732; Commandes directes [www.surfaces-utiles.org](http://surfaces-utiles.org/publi-fluor-affaires-de-lettres-a-bruxelles-letterzaken-in-brussel-letter-business-in-brussels.html)


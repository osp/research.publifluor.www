Title: Backcover-en
Date: 2024-12-03 10:20 
Authors: Olivier Bertrand 
Summary: résumé  
Slug: backcover
Lang: en


Between 1975 and 2000, the autodidact Chrystel Crickx cut letters by
hand and sold them by the piece in her shop in Schaærbeek, Publi Fluor.
These vinyl adhesives, bought by locals for their signs and advertising,
have since been digitized and made accessible to users outside Belgium
and in a range of new contexts.

On the fringes of standardized communication, the letterforms have
contributed and continue to contribute to the urban visual environment
in Brussels and beyond. This non-standard collective essay attempts to
tell the life of a type model, its successive authors and their tools,
all while broadening the field and exploring the interstices between the
many stories that Chrystel Crickx's practice gave rise to.


<!-- ### Distribution -->

- 384 pages, 16.8 × 23 cm
- FR/NL/EN
- &#127799; 7 colors, softcover, swiss binding
- ISBN 978-2-931110-10-2
- 37€
- &#9732; Direct orders [www.surfaces-utiles.org](http://surfaces-utiles.org/publi-fluor-affaires-de-lettres-a-bruxelles-letterzaken-in-brussel-letter-business-in-brussels.html)
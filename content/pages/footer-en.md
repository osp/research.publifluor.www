Title: Footer
Date: 2024-12-03 10:20 
Authors: Olivier Bertrand 
Summary: résumé  
Slug: footer
Lang: en
 
 
Publi Fluor is a project led by the Groupe de recherche Crickx, composed of Sophie Boiron (graphic designer, cartographer
and typographer, guardian of the Publi Fluor archive), Olivier Bertrand (publisher, graphic designer, typog-
rapher and educator), Pierre Huyghebaert (graphic designer, typographer, cartographer, educator and guardian of the Publi Fluor archive), David Le Simple (bookseller specializing in picture books, and educator), Ludivine Loiseau (typographer, designer and teacher), Nathan Izbicki (artist, photographer) and Femke Snelting (artist, researcher, author). 

Website designed by OSP, Ludi with the help of Doriane

Contact us at [crickx@speculoos.com](mailto:crickx@speculoos.com)
Supported by FWB and VGC

![State_of_the_art_logo](/images/State_of_the_art_horizontal_yellow_full_version.svg)

![FWB logo](/images/logo-fwb-horizontal.svg)
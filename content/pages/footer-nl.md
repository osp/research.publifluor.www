Title: Footer
Date: 2024-12-03 10:20 
Authors: Olivier Bertrand 
Summary: résumé  
Slug: footer
Lang: nl
 
Publi Fluor is een project van de onderzoeksgroep Crickx, bestaande uit Sophie Boiron (grafisch ontwerpster, cartografe en typografe, bewaardster van het Publi Fluor-archief), Olivier Bertrand (uitgever, grafisch ontwerper, typograaf en docent), Pierre Huyghebaert (grafisch ontwerper, typograaf, cartograaf, docent en bewaarder van het Publi Fluor-archief), David Le Simple (gespecialiseerde boekhandelaar in geïllustreerde boeken en docent), Ludivine Loiseau (grafiste, typografe en docente), Nathan Izbicki (kunstenaar,fotograaf) et Femke Snelting (kunstenares, onderzoeker). 

Website ontworpen door OSP, Ludi met de hulp van Doriane

Neem contact met ons op via [crickx@speculoos.com](mailto:crickx@speculoos.com)
Ondersteund door FWB en VGC

![State_of_the_art_logo](/images/State_of_the_art_horizontal_yellow_full_version.svg)

![FWB logo](/images/logo-fwb-horizontal.svg)
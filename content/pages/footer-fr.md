Title: Footer
Date: 2024-12-03 10:20 
Authors: Olivier Bertrand 
Summary: résumé  
Slug: footer
Lang: fr
 
 
Publi Fluor est un projet mené par le Groupe de recherche Crickx, composé de Sophie Boiron (typographe, graphiste, cartographe et gardienne de l'archive Publi Fluor), Olivier Bertrand (éditeur, designer, enseignant), Pierre Huyghebaert (designer, typographe, cartographe, enseignant et gardien de l'archive Publi Fluor), David Le Simple (libraire, éditeur et enseignant), Ludivine Loiseau (typographe, designer et enseignante), Nathan Izbicki (artiste, photographe) et Femke Snelting (chercheuse). 

Site web dessiné par OSP, Ludi avec l'aide de Doriane

Contact us at [crickx@speculoos.com](mailto:crickx@speculoos.com)
Supported by FWB and VGC

![State_of_the_art_logo](/images/State_of_the_art_horizontal_yellow_full_version.svg)

![FWB logo](/images/logo-fwb-horizontal.svg)
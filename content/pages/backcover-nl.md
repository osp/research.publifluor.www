Title: Achterflap
Date: 2024-12-03 10:20 
Authors: Olivier Bertrand 
Summary: résumé  
Slug: backcover
Lang: nl

Chrystel Crickx, beletteraar en typografe, is autodidacte. In haar
winkel Publi Fluor in Schaarbeek sneed ze letters met de hand om ze
vervolgens per stuk te verkopen. Tussen 1975 en 2000 sneed ze de
felgekleurde letters uit zelfklevend vinyl voor lokale bewegwijzering en
reclame op winkelruiten. Sindsdien zijn ze gedigitaliseerd en op bredere
schaal beschikbaar gemaakt voor gebruik buiten België en in nieuwe
contexten.

In de marge van genormaliseerde communicatie droegen en dragen de
letters zo bij aan de stedelijke visuele omgeving in Brussel en elders.
Deze ongewone collectieve publicatie vertelt over het leven van die
lettervormen, en van de betrokken auteurs en hun diverse gereedschappen.
Hij verbreedt de horizon om uit te zoeken wat er zich afspeelt in de
ruimtes tussen de verschillende verhalen die de praktijk van Chrystel
Crickx oproept.

<!-- ### Distributie -->

- 384 pagina's, 16,8 × 23 cm
- FR/NL/EN
- &#127799; 7 kleuren, zachte kaft, zwitserse binding
- ISBN 978-2-931110-10-2
- 37€
- &#9732; Direct bestellen [www.surfaces-utiles.org](http://surfaces-utiles.org/publi-fluor-affaires-de-lettres-a-bruxelles-letterzaken-in-brussel-letter-business-in-brussels.html)

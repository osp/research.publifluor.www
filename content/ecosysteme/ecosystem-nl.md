Title: Ecosysteem
Date: 2024-12-04 10:20
Authors: Crickx onderzoeksgroep
Summary: résumé
Slug: ecosysteme
Lang: nl


### Belangrijke data

- [1910](#1910)
- [1916](#1916)
- [1939](#1939)
- [1943](#1943)
- [1944](#1944)
- [1953](#1953)
- [1957](#1957)
- [1958](#1958)
- [1961](#1961)
- [1961](#1965)
- [1968](#1968)
- [1969](#1969)
- [1970](#1970)
- [1972](#1972)
- [1973](#1973)
- [1974](#1974)
- [1980](#1980)
- [1984](#1984)
- [1985](#1985)
- [1989](#1989)
- [1991](#1991)
- [1994](#1994)
- [1996](#1996)
- [1997](#1997)
- [1998](#1998)
- [1999](#1999)
- [2000](#2000)
- [2001](#2001)
- [2003](#2003)
- [2004](#2004)
- [2006](#2006)
- [2007](#2007)
- [2010](#2010)
- [2011](#2011)
- [2013](#2013)
- [2014](#2014)
- [2016](#2016)
- [2017](#2017)
- [2019](#2019)
- [2020](#2020)
- [2021](#2021)
- [2022](#2022)
- [2023](#2023)
- [2024](#2024)

**1910**<a id='1910'></a> Geboorte van Raymond, de vader van Chrystel in Doornik.
**1916**<a id='1916'></a> Geboorte van Alberte (Betty genaamd), Chrystels moeder, in Gent.
**1939**<a id='1939'></a> Geboorte van Chrystels broer.
**1943**<a id='1943'></a> Geboorte van Chrystel. **1944**<a id='1944'></a> Geboorte van Marie-Claude, Chrystels
zus.
**1953**<a id='1953'></a> Raymond opent zijn eerste atelier of winkel in een huis op de
Vifquinstraat in Schaarbeek (Brussel), waarschijnlijk nr. 10. De
belettering sneed hij met de hand uit vinyl, terwijl de meeste
beletteraars rechtstreeks met een penseel op de ondergrond werkten.
**1957**<a id='1957'></a> De winkel en de familie verhuizen van de Vifquinstraat naar de
Wijnheuvelenstraat op een onbekend nummer.
**1957** Chrystel gaat van school op 14-jarige leeftijd.
**1958**<a id='1958'></a> Geboorte van Marcelline, de zus van Chrystel. De winkel en het
gezin verhuizen naar de Rogierlaan, op nummer 80, onder de naam
Fluoréclam.

![]({static}/images/ecosystem/ecosystem_fig-1.jpg)


**1961**<a id='1961'></a> Op 18-jarige leeftijd trouwt Chrystel voor de eerste keer. Ze woont
in Oostende en werkt in een kruidenierszaak geopend door haar man, en
daarna als zaalwachter in een bioscoop. Uiteindelijk opent Chrystel een
dancing-bowlingbar, 't Mandje.
**1965**<a id='1965'></a> Chrystel verlaat Oostende en ook haar man op 22-jarige leeftijd. Ze
werkt als arbeidster bij de banketbakker Bonbons Antoine in Elsene. Haar
ambachtelijke vaardigheden worden opgemerkt door de baas, die haar
aanstelt als industrieel ontwerpster van snoepjes.
**1968**<a id='1968'></a> 1968 Chrystel ontmoet de vader van haar eerste zoon; ze wonen op de
Wijnheuvelenstraat.
**1969**<a id='1969'></a> De geboorte van Chrystels eerste zoon, Serge, gevolgd door de
scheiding van het koppel. Ze ontmoet René Crickx en trouwt met hem.
**1970**<a id='1970'></a> Geboorte van Michel, de tweede zoon van Chrystel. Roger Nols wordt
ondertussen verkozen tot burgemeester van Schaarbeek en voert een
populistisch, anti-flamingantisch en racistisch rechts beleid door. De
gemeente vormt één van de uiteinden van de arme sikkel van Brussel.
**1972**<a id='1972'></a> Raymond wordt ziek en de drie zussen werken enkele maanden samen in
de winkel.

![]({static}/images/ecosystem/ecosystem_fig-2.jpg)

**1973**<a id='1973'></a> Raymond overlijdt. Chrystel neemt de winkel over en werkt samen met
haar zus of zussen. De collages op de gevel van de winkel van Chrystels
vader maken de eigenaar van het gebouw boos, dus verhuist ze de zaak
naar nr. 74 in dezelfde straat. Haar zus Marie-Claude heeft
een cadeauwinkel op nr. 72.
**1974**<a id='1974'></a> Op 500 meter van de winkel worden De Hallen van Schaarbeek
omgebouwd tot een cultureel centrum.
**1974** Chrystel koopt het pand aan de Rogierlaan 102 en vestigt zich daar
onder de nieuwe naam Publi Fluor. Ze snijdt letters uit vinyl en
verkoopt ze per stuk, zonder ze te plaatsen.
**1980**<a id='1980'></a>1980 Marktintroductie van de eerste snijplotters, computergestuurde
machines.
**1984**<a id='1984'></a> In navolging van Xerox, marketing en ontwikkeling van grafische
microcomputers door Apple, Microsoft, Adobe en PostScript. Geleidelijke
maar snelle toename van gebruik door ambachtlieden die dagelijks letters
tekenen of zetten.
**1985**<a id='1985'></a> Naast andere, voornamelijk met de hand geschilderde belettering
ziet Pierre Huyghebaert de letters van Chrystel op etalages in
Schaarbeek zonder de herkomst ervan te kennen.
**1989**<a id='1989'></a> Einde van het burgemeesterschap van Roger Nols. België wordt een
federale staat en het Brussels Hoofdstedelijk Gewest wordt opgericht.
**1991**<a id='1991'></a> Pierre begint een experimentele letteruitgeverij met Misch Dimmer
en Karl Bassil tijdens hun studies aan de erg (École de recherche
graphique, Elsene) onder de naam Hammerfonts Foundry.
**1994**<a id='1994'></a> Na het Nols-tijdperk komen er verkozenen van Marokkaanse en
Albanese afkomst in de gemeenteraad van Schaarbeek.
**1996**<a id='1996'></a> De renovatie van de Hallen van Schaarbeek wordt na jaren werken
afgerond. Pierre werkt nu samen met beeldend kunstenaar Vincent
Fortemps, samen ontwikkelen ze de grafische identiteit van de zaal en
ontwerpen ze boekjes en affiches.
**1997**<a id='1997'></a> Voor de affiches voor een filmfestival met Indiase films \[Fig. 3\]
overwegen Pierre en Vincent om de uitgesneden letters te gebruiken die
veel voorkomen op de etalages rondom de Hallen, ondanks het toenemende
gebruik van gestandaardiseerde typografie elders. Ze gaan op zoek naar
de bron en Pierre vindt de Publi Fluor-winkel. Pierre ontmoet Chrystel
en koopt een set van 26 letters en 10 cijfers in 'normale' stijl (het
meest voorkomende cursief), 4 cm hoog. Hij scant ze in en vectoriseert
ze automatisch met behulp van Streamline software (later door Adobe
geïntegreerd in Illustrator). Met deze gevectoriseerde letters stelt hij
snel de titel van de poster samen. Pierre maakt vervolgens het eerste
digitale lettertype, dat hij Crickx Rush noemt, en dat hij gebruikt
gebruikt voor andere affiches van de Hallen van Schaarbeek, flyers, programma's en logo's.

![pierre-huyghebaert-vincent-fortemps-new-sound-of-india-halles-de-schaerbeek]({static}/images/ecosystem/S-fig32.jpg)

**1998**<a id='1998'></a> Jan Middendorp, journalist en redacteur van FontShop België's
typografische tijdschrift *Druk*, schrijft een artikel over het
grafische werk van Pierre en Vincent voor de Hallen. Pierre gebruikt het
lettertype voor projecten van Fréon. Pierre geeft het lettertype om
praktische redenen door aan een paar mensen in zijn omgeving, zonder de
licentie te verduidelijken.
**1999**<a id='1999'></a> Jan geeft Pierre de opdracht om een artikel van 5 pagina's over
Chrystel te schrijven voor *Druk*. Hij schrijft het in het Frans, zet
het op papier en Jan vertaalt het in het Nederlands. Wanneer *Druk* een
overzicht maakt van de artikelen die de meeste belangstelling hadden
getrokken, komt het artikel over Chrystel als meest gelezen uit de bus.
Chrystel sluit de winkel, 'Ik doe dit al 26 jaar \[letters
uitsnijden\]'. Tussen Kerst en Nieuwjaar 2000 overlijdt René Crickx.
**2000**<a id='2000'></a> Chrystel gaat met pensioen en verhuist naar een huis in Wallonië.
**2001**<a id='2001'></a> Speculoos, de studio waarin Pierre samenwerkt met twee andere
mensen, koopt voor een symbolische prijs alle overgebleven zelfklevende
letters evenals het archief, vooral om te voorkomen dat ze vernietigd
worden. Aan het eind van datzelfde jaar bestelt Pierre bij Chrystel
kleine letters, leestekens en symbolen om de set compleet te maken. Ze
produceert de versie die later door OSP Blobby zal worden genoemd.
**2003**<a id='2003'></a> Het lettertype Crickx-rush-light-ext (een van de geoptimaliseerde
versies, geproduceerd door Speculoos voor gebruik in Flash-websites),
toegeschreven aan Hammerfonts, lekt via het Belgisch-Franse netwerk van
hedendaagse stripuitgevers en verschijnt in het Parijse stripblad
*Bang!*.

![bang-magazine]({static}/images/ecosystem/S-fig115.jpg)

**2004**<a id='2004'></a> De letters worden tentoongesteld in de etalage van Speculoos aan de
Charleroisesteenweg in Sint-Gillis.

**2006**<a id='2006'></a> Harrisson, Femke Snelting en Nicolas Malevé richten Open Source
Publishing (OSP) op. Ludi Loiseau komt in Brussel aan via de École
nationale des arts visuels de La Cambre, waar ze Pierre ontmoet, die er
al drie jaar les geeft. Een tweede *Print party* door Harrisson en Femke
in La Quarantaine in Elsene wordt bijgewoond door Pierre en Ludi,
waarbij het publiek kan zien hoe stap voor stap een boekje wordt
geproduceerd met behulp van verschillende vrije softwareprogramma's.
Ludi begint te werken voor Speculoos. Pierre sluit zich aan bij OSP voor
een *Print party* in Berlijn met Harrisson, waar hij Femke vervangt.
**2007**<a id='2007'></a> Ludi gaat met OSP mee naar de Libre Graphics Meeting in Wrocaw
(Polen), waar de eerste OSP lettertypes worden ontworpen en vervolgens
gepubliceerd onder de SIL Open Font License (OFL).
**2010**<a id='2010'></a> Ludi en Antoine Begon, dan stagiair bij Speculoos en OSP,
herontwerpen de Crickx-lettertypes en publiceren ze op de OSP-foundry en
op andere vrije typografieplatforms. Crickx begint op grotere schaal te
circuleren.
**2011**<a id='2011'></a> Sophie Boiron sluit zich aan bij studio Speculoos. Opening van de
Constant Variable hackerspace in Schaarbeek, op 1 km van waar Publi
Fluor zich bevond. Variable brengt artistieke praktijken samen die
geïnspireerd zijn door vrije software, en OSP opent er zijn eerste
studio. Het Crickx archief is erheen verhuisd en vormt de basis voor de
grafische identiteit van Variable. Chrystel bezoekt de ruimte tijdens de
officiële, en feestelijke opening. Het archief wordt toegankelijk
gemaakt voor het publiek.
**2013**<a id='2013'></a> Variable sluit af met een tentoonstelling van een deel van het
Crickx-archief georganiseerd in het naburige cultureel centrum De
Kriekelaar, daarna verhuist het archief naar de nieuwe kantoren van OSP
op de 26<sup>e</sup> verdieping van één van de twee WTC-torens, in Brussel.
**2014**<a id='2014'></a>2014 Femke schrijft 'Vandaag begon ze met C'.
**2016**<a id='2016'></a> Niet-geregistreerd optreden voor het tijdschrift Médor (waarvan
Ludi en Pierre medeoprichters zijn) voor Live Magazine in het Théâtre
National, met een korte lezing van Pierre over het 'Crickx-verhaal'.
**05.04.2017**<a id='2017'></a> Papier Carbone Festival in Charleroi. Ludi en Stéphanie
Vilayphiou van OSP snijden de 'autotrace versie' Crickx letters uit
vinyl om ze aan te bieden aan het publiek.
**2017** Op initiatief van Sophie verhuist het archief naar de nieuwe ruimte
van Spec uloos (hernoemd met een spatie tussen de twee delen van de
naam), Elewyckstraat 47 in Elsene.
**2019**<a id='2019'></a> Overlijden van de eerste zoon van Chrystel.
**10.2019** Begin van de tentoonstelling van mallen in de Specu loos etalage
ter gelegenheid van We Art XL.
**2020**<a id='2020'></a> Bloemisterij Nouveau opent haar deuren in Elsene en gebruikt enkel
Crickx voor haar communicatie.
**2020** De projectoproep 'Un Futur pour la Culture' wordt gelanceerd door
De Fédération Wallonie-Bruxelles (ook wel de Communauté Francaise
genoemd). Surfaces Utiles ziet een kans om onderzoek te financieren dat
zal kunnen leiden tot de publicatie van een boek, gebaseerd op het Publi
Fluor-archief. De Crickx Onderzoeksgroep wordt opgericht na de
toekenning van de subsidie.
**12.01.2021**<a id='2021'></a> Eerste vergadering van de Crickx Onderzoeksgroep.
**09-13.03.2021** Olivier Bertrand en David Le Simple lanceren het Laboratoire
de recherche-création Crickx bij ESA Le 75 rond het Publi Fluor-archief,
met een transdisciplinair team van studenten. Openingslezing door Pierre
en Sophie, als beheerders van het archief.
**09-13.03.2021** Uitpakken van Publi Fluor-workshop bij Spec uloos door
studenten en de onderzoeksgroep. Tentoonstelling in het atelierraam van
belettering en stukken uit het archief \[Fig. 5\].
**31.03.2021** Typografische speurtocht van het lab op zoek naar sporen van
Crickx-letters in de Rogierlaan, Schaarbeek.
**21.04.2021** Interventie van kunstenaar-archivaris Mathieu Gargam in het
Laboratoire de recherche-création.
**29.09.2021** Lancering van het eerste verslag over het Laboratoire de
recherche-création Crickx, Rue Crickx in Sint-Gillis (een relatief veel
voorkomende familienaam in Brussel). Belettering van een raam voor de
gelegenheid.
**09.2021** Lancering van de oproep 'Cherche amateurix de la Crickx' \[op
zoek naar Crickx-liefheb·b·er·s\] om verschillende toepassingen van
Crickx te documenteren.
**20-21.10.2021** Touring Club gaat op zoek naar andere sporen van letters
in de stad. Studenten van Le 75, David, Ludi, Olivier en Sophie
interviewen enkele Crickx-gebruikers.
**16.11.2021** Eerste interview met Chrystel door de onderzoeksgroep.
**20.04.2022**<a id='2022'></a> Presentatie van de onderzoeks- en creatieprojecten van de Le
75 studenten in aanwezigheid van Mathieu Gargam.
**07.05.2022** Afsluitende workshop van het Laboratoire de
recherche-création in aanwezigheid van grafisch ontwerper Axel Benassis.
Constructie van een specimen op ware grootte op basis van de tastbare
letters van Chrystel Crickx voor een nummer van het
typografietijdschrift *La Perruque*.
**09.2022** Nathan Izbicki, oud-student aan ESA Le 75 en lid van het
laboratorium, sluit zich aan bij de Crickx Onderzoeksgroep.
**20.11.2022** Tweede interview met Chrystel, vergezeld door haar twee
zussen.
**01.2023**<a id='2023'></a> Ontvangst van een beurs van de Vlaamse Gemeenschap voor het
boekproject en de typografische projecten van de Crickx Onderzoeksgroep.
**06.03.2023** Tijdens een lezing op basis van haar essay 'Mijn Letters'
licht Femke de auteurs- en licentieproblematiek van Crickx toe in het
kader van het symposium 'Post\$cript, écoles sous licence' aan de erg.
**04.2023** Ontvangst van een publicatiebeurs van de Fédération
Wallonie-Bruxelles voor dit werk.
**08-12.05.2023** Residentie van de onderzoeksgroep in het atelier van
Constant aan de Jetsesteenweg in Koekelberg.
**25-26.05.2023** De onderzoeksgroep geeft de workshop 'Resharpening Blobby'
in het kader van de Ultradependent Public School bij BAK, Utrecht.
**10.06.2023** Chrystel bezoekt samen met de onderzoeksgroep de voormalige
adressen van de Fluoréclam en Publi Fluor-winkels en -werkplaatsen in
Schaarbeek.
**11-15.09.2023** Residentie van de onderzoeksgroep in Meyboom Artist-Run
Spaces, Brussel.
**04.2024**<a id='2024'></a> Lancering van het boek Publi Fluor, letterzaken in Brussel in de
boekhandel Brin d'acier in Schaarbeek. Heruitgave van het digitale
lettertype onder de naam Publi Fluor.

### Veel voorkomende namen

**Brussel **

Belgische stad en hoofdstad van België. Ze maakt deel uit van het
Brussels Hoofdstedelijk Gewest, één van de drie gewesten waaruit België
bestaat, dat op zijn beurt is samengesteld uit negentien gemeenten. In
dit boek komen we de gemeenten Schaarbeek, Anderlecht, Brussel-Stad,
Vorst, Sint-Jans-Molenbeek, Sint-Gillis, Elsene en Sint-
Lambrechts-Woluwe tegen. In het dagelijkse taalgebruik wordt de naam
Brussel vaak ten onrechte gebruikt om het hele Brussels Hoofdstedelijk
Gewest aan te duiden.

**ESA Le 75 **

De École Supérieure des Arts de l'image Le 75 bevindt zich in de
gemeente Sint-Lambrechts- Woluwe. De kunstschool biedt een
bacheloropleiding aan in vier richtingen: grafisch ontwerp, meervoudig
gedrukte beelden, schilderkunst en fotografie.

**Crickx onderzoeksgroep **

Publi Fluor is een project dat wordt geleid door de Crickx
onderzoeksgroep, bestaande uit Sophie Boiron (typograaf, grafisch
ontwerper, cartograaf en beheerder van het Publi Fluor-archief), Olivier
Bertrand (uitgever, ontwerper, docent), Pierre Huyghebaert (ontwerper,
typograaf, cartograaf, docent en beheerder van het Publi Fluor-archief),
Nathan Izbicki (kunstenaar, fotograaf), David Le Simple (boekhandelaar,
uitgever en docent), Ludivine Loiseau (typograaf, ontwerper en docent)
en Femke Snelting (onderzoeker). Het onderzoek van de groep heeft geleid
tot dit boek en deze hernoeming en uitbreiding van de digitale
glyph-sets van Crickx.

**Laboratoire de recherche-création Crickx **

Een onderzoeks- en creatielaboratorium opgezet rond het Publi
Fluor-archief door een transdisciplinaire groep studenten en docenten
van ESA Le 75 tussen eind 2020 en mei 2022. Leden: Soazig Auvray,
Camille Balseau, Olivier Bertrand, Pauline Barret, Leyla Cabaux, Abigaël
Coeffier, Léonard Gensane, Nathan Izbicki, David Le Simple, Lysiane
Schwab, Élise Tanguy.

**Fréon **

Fréon is een hedendaagse stripuitgeverij die begin jaren negentig werd
opgericht door Thierry Van Hasselt, Vincent Fortemps, Olivier Poppe,
Olivier Deprez en Denis Deprez, nadat ze elkaar in het stripatelier van
de kunstacademie Saint-Luc in Brussel hadden ontmoet. De boeken die
Fréon uitgeeft zijn beperkt in aantal, maar redactioneel complex
en beeldend rijk. In 2002 fuseerde Fréon met de Franse uitgever Amok
tot FRMK (spreek uit als Frémok).

**Bye Bye Binary (BBB) **

BBB is een Frans-Belgisch collectief wiens acties het debat aanwakkeren
over de politieke lading van grafisch ontwerp, taal en representaties
van lichamen en identiteiten. BBB kiest voor militant en
gemeenschapsgericht onderzoek: door en met de mensen waar het om gaat en
door een feministisch, queer, trans\*, queer, bi, lesbisch prisma. Sinds
2021 publiceert het een letterbibliotheek die een verzameling
post-binaire typografische tekens samenbrengt en verspreidt voor gebruik
door zoveel mogelijk mensen.

**Open Source Publishing (OSP) **

OSP is een collectief van grafisch ontwerpers gevestigd in Brussel. Hun
materialen zijn typografie, websites, web-to-print tools en plotters. Ze
bevragen de invloed en mogelijkheden van digitale tools door middel van
(grafisch) ontwerp in opdracht, onderwijs en toegepast onderzoek. Door
middel van hun projecten, waarbij ze uitsluitend Free and Open Source
software (F/LOSS) gebruiken, bevragen ze software als culturele objecten
en de manieren waarop grafisch ontwerpers, kunstenaars, culturele
instellingen en scholen samenwerken.

**Speculoos/Spec/Spec uloos **

Spec uloos is een studio gevestigd in Brussel. Sinds de oprichting in
2002, door Pierre Huyghebaert, François Dispaux en Alexia de Visscher,
zijn de letters van de naam in beweging geweest. De studio wordt
momenteel geleid door Pierre Huyghebaert en Sophie Boiron, in
samenwerking met verschillende freelancers. Spec werkt voornamelijk in
de culturele, associatieve en publieke sfeer en smeedt banden tussen het
Brusselse culturele netwerk, spelers op het gebied van architectuur en
verschillende organisaties die betrokken zijn bij stadsprojecten,
uitgeverijen, theaterorganisaties en kunstenaars. Het bureau is
betrokken bij een reeks eclectische maar verwante onderzoeksprojecten,
op het gebied van bijvoorbeeld typografie, uitgeverij, co-ontwerp van
tentoonstellingen, ontwerp van bewegwijzering, en cartografie, met name
binnen de coöperatie Atelier cartographique.

**Constant **

Constant is een vereniging die sinds 1997 in Brussel gevestigd is, en en
actief is op het gebied tussen kunst, media en technologie. Constant
werkt met feministische servers, experimentele publicaties, actieve
archieven, extitutionele netwerken, (her)leersituaties, hackbare
apparaten, performatieve protocollen, solidaire infrastructuren en
andere sponsachtige praktijken die paden uitstippelen naar speculatieve,
vrije en intersectionele technologieën.

**Variable **

Van 2011 tot 2014 had Constant in Schaarbeek een gebouw van de Vlaamse
Gemeenschap ter beschikking. Onder de naam Variable boden zij onderdak
aan studio's voor kunstenaars, ontwerpers, techno-uitvinders, data-
activisten, cyber-feministen, interactieve geeks, textielpiraten,
videografen, geluidsenthousiastelingen, beat-makers en andere digitale
makers die geïnteresseerd waren in het gebruik van vrije en Open Source
software, waaronder OSP.
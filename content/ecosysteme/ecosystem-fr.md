Title: Écosystème
Date: 2024-12-04 10:20
Authors: Groupe de recherche Crickx
Summary: résumé
Slug: ecosysteme
Lang: fr

### Dates clés

- [1910](#1910)
- [1916](#1916)
- [1939](#1939)
- [1943](#1943)
- [1944](#1944)
- [1953](#1953)
- [1957](#1957)
- [1958](#1958)
- [1961](#1961)
- [1961](#1965)
- [1968](#1968)
- [1969](#1969)
- [1970](#1970)
- [1972](#1972)
- [1973](#1973)
- [1974](#1974)
- [1980](#1980)
- [1984](#1984)
- [1985](#1985)
- [1989](#1989)
- [1991](#1991)
- [1994](#1994)
- [1996](#1996)
- [1997](#1997)
- [1998](#1998)
- [1999](#1999)
- [2000](#2000)
- [2001](#2001)
- [2003](#2003)
- [2004](#2004)
- [2006](#2006)
- [2007](#2007)
- [2010](#2010)
- [2011](#2011)
- [2013](#2013)
- [2014](#2014)
- [2016](#2016)
- [2017](#2017)
- [2019](#2019)
- [2020](#2020)
- [2021](#2021)
- [2022](#2022)
- [2023](#2023)
- [2024](#2024)


**1910**<a id='1910'></a> Naissance de Raymond, père de Chrystel, à Tournai.
**1916**<a id='1916'></a> Naissance d’Alberte (appelée Betty), mère de Chrystel, à Gand.
**1939**<a id='1939'></a> Naissance du frère de Chrystel. 
**1943**<a id='1943'></a> Naissance de Chrystel.**1944**<a id='1944'></a> Naissance de Marie-Claude, sœur de Chrystel.
**1953**<a id='1953'></a> Premier atelier ou magasin de Raymond, dans une maison de la rue Vifquin à Schaerbeek (Bruxelles), probablement au numéro 10. Les lettrages
produits sont basés sur la découpe du vinyle à la main, alors qu’une grande majorité des lettreur·ses travaillent en direct au pinceau sur le
support.
**1957**<a id='1957'></a> Le magasin et la famille déménagent de la rue Vifquin à la rue des Coteaux à un numéro non identifié.
**1957** Chrystel quitte l’école à 14 ans.
**1958**<a id='1958'></a> Naissance de Marcelline, sœur de Chrystel. Le magasin et la famille déménagent à l’avenue Rogier, au numéro 80, sous le nom de Fluoréclam

![]({static}/images/ecosystem/ecosystem_fig-1.jpg)

**1961**<a id='1961'></a> Premier mariage de Chrystel à 18 ans. Elle vit à Ostende et travaille dans une épicerie ouverte par son mari, puis elle travaille comme ouvreuse de cinéma. Enfin, Chrystel ouvre un bar-dancing-bowling, le *’t Mandje*. 
**1965**<a id='1965'></a> Chrystel quitte Ostende et son mari à 22 ans. Chrystel travaille chez le confiseur Bonbons Antoine à Ixelles comme ouvrière. Ses qualités artisanales sont remarquées par le patron qui la nomme au dessin industriel de bonbons. 
**1968**<a id='1968'></a> Chrystel rencontre le père de son premier fils, ils vivent rue des Coteaux. 
**1969**<a id='1969'></a> Naissance du premier fils de Chrystel, Serge, puis séparation du
couple. Elle rencontre René Crickx et l’épouse. 
**1970**<a id='1970'></a> Naissance de Michel, deuxième fils de Chrystel. Pendant ce temps,
Roger Nols est élu bourgmestre de Schaerbeek et mène dans cette commune,
constituant une extrémité du croissant pauvre de Bruxelles, une
politique de droite populiste, anti-flamingante et raciste. 
**1972**<a id='1972'></a> Raymond tombe malade, et les trois sœurs travaillent ensemble dans
le commerce pour quelques mois. 
**1973**<a id='1973'></a> Mort de Raymond. Chrystel reprend le magasin et travaille avec sa ou ses sœurs. 

![]({static}/images/ecosystem/ecosystem_fig-2.jpg)

Les collages du père sur la façade provoquent
le méconten­tement du propriétaire du bâtiment, elle déménage alors le
commerce au numéro 74 de la même rue. Sa sœur Marie-Claude tient une
boutique de cadeaux au numéro 72.
**1974**<a id='1974'></a> Les Halles de Schaerbeek sont transformées en lieu culturel, à 500
mètres du magasin.
**1974** Chrystel achète la maison avenue Rogier 102 et y installe le
commerce sous un nouveau nom, *Publi Fluor*. Elle y découpe des lettres et
les vend à la pièce, sans placement.
**1980**<a id='1980' class='anchors'></a> Commercialisation des premiers traceurs de découpe, machines
contrôlées par ordinateur.
**1984**<a id='1984'></a> À la suite de Xerox, commercialisation et développement de la
micro-informatique graphique avec Apple, Microsoft, Adobe et le
PostScript. Équipement progressif mais très rapide des artisan·es dont
le dessin ou la composition des lettres est le quotidien.
**1985**<a id='1985'></a> Pierre Huyghebaert repère les lettres de Chrystel sur les vitrines
de Schaerbeek sans connaître leur origine, parmi d’autres lettrages,
pour la plupart encore peints.
**1989**<a id='1989'></a> Fin du mandat de bourgmestre de Roger Nols. Fédéralisation de la
Belgique et création de la Région de Bruxelles-Capitale.
**1991**<a id='1991'></a> Pierre commence à produire des fontes expérimentales avec Misch
Dimmer et Karl Bassil pendant leurs études à l’erg (École de recherche
graphique, Ixelles), elles sont signées fonderie Hammerfonts.
**1994**<a id='1994'></a> Après l’ère Nols, des élu·es d’origines marocaine et albanaise font
leur entrée au Conseil communal de Schaerbeek.
**1996**<a id='1996'></a> Fin de la rénovation des Halles de Schaerbeek après des années de
travaux. Pierre travaille désormais en duo avec le plasticien Vincent
Fortemps et ils développent ensemble l’identité graphique du lieu à
travers livrets et affiches.
**1997**<a id='1997'></a> Pour les affiches d’un festival de cinéma indien, 

![pierre-huyghebaert-vincent-fortemps-new-sound-of-india-halles-de-schaerbeek]({static}/images/ecosystem/S-fig33.jpg)

Pierre et Vincent envisagent d’utiliser le lettrage des lettres découpées qui
sont courantes sur les vitrines aux alentours des Halles, malgré l’usage
de plus en plus massif de typographies standardisées par ailleurs. Ils
cherchent leur provenance et Pierre trouve le magasin *Publi Fluor*.
Pierre rencontre Chrystel. Il y achète un set de 26 lettres et
10 chiffres de style « normal » (l’italique le plus courant) en 4 cm de
hauteur, les scanne et réalise une vectorisation automatique dans le
logiciel Streamline (par après intégré par Adobe à Illustrator). Avec
ces lettres vectorisées, il compose à la va-vite le titre de l’affiche.
Pierre produit ensuite la première fonte numérique qu’il baptise Crickx
Rush. Utilisation pour d’autres affiches des Halles de Schaerbeek, flyers, programmes, logos.

![pierre-huyghebaert-vincent-fortemps-new-sound-of-india-halles-de-schaerbeek]({static}/images/ecosystem/S-fig32.jpg)


**1998**<a id='1998'></a> Jan Middendorp, journaliste et éditeur du magazine typographique
*Druk* de FontShop Belgique, fait un article sur Pierre et Vincent à
propos de leurs productions graphiques pour les Halles. Pierre utilise
la fonte pour des projets de Fréon. Pierre transmet la fonte à quelques
personnes entre autres de ce milieu pour des raisons pratiques, sans que
sa licence soit clarifiée
**1999**<a id='1999'></a> Jan commande à Pierre un article de 5 pages sur Chrystel pour
*Druk*. Il l’écrit en français, le met en page et Jan le traduit en
néerlandais. Lorsque *Druk* fait une enquête sur les articles qui ont
rencontré le plus d’intérêt, celui sur Chrystel arrive en tête. Chrystel
ferme le magasin, « j’ai fait ça pendant 26 ans \[découper des
lettres\] ». Entre Noël et le Nouvel An 2000, René Crickx décède.
**2000**<a id='2000'></a> Chrystel prend sa retraite et déménage vers une maison en Wallonie.
**2001**<a id='2001'></a> Le studio dans lequel Pierre s’est associé avec deux autres
personnes, Speculoos, achète toutes les lettres autocollantes restantes,
ainsi que les archives, pour un prix symbolique et surtout pour en
éviter la destruction. À la fin de la même année, Pierre passe commande
de bas de casse, ponctuation et symboles à Chrystel pour compléter le
set de glyphes. Elle produit la version qui sera appelée plus tard
Blobby par OSP.
**2003**<a id='2003'></a> La police Crickx-rush-light-ext (l’une des nombreuses versions
optimisées produites par Speculoos pour être utilisées dans des sites
web en Flash), attribuée à Hammerfonts, fait l’objet d’une fuite à
travers le réseau belgo-français des éditeur·ices de bande dessinée
contemporaine et apparaît dans le magazine de BD parisien *Bang !*.

![bang-magazine]({static}/images/ecosystem/S-fig115.jpg)


**2004**<a id='2004'></a> Les lettres Crickx sont exposées dans la vitrine de Speculoos à la
chaussée de Charleroi, à Saint-Gilles.
**2006**<a id='2006'></a> Harrisson, Femke Snelting et Nicolas Malevé fondent Open Source
Publishing (OSP). La « caravane » est ancrée dans la pratique de
Constant, Ludi Loiseau arrive à Bruxelles par l’École nationale des arts
visuels de La Cambre et y rencontre Pierre qui y donne cours depuis
3 ans. Seconde *print party* par Harrisson et Femke à La Quarantaine à
Ixelles, à laquelle assistent Pierre et Ludi et pendant laquelle un
livret est entièrement produit en public en utilisant différents
logiciels libres. Ludi commence à travailler pour Speculoos. Pierre
rejoint OSP pour une *print party* à Berlin avec Harrisson en
remplacement de Femke.
**2007**<a id='2007'></a> Ludi rejoint OSP à l’occasion d’un voyage vers le Libre Graphics
Meeting à Wrocaw (Pologne) à l’occasion duquel sont dessinées les
premières fontes OSP, publiées ensuite sous licence libre SIL Open Font
License (OFL).
**2010**<a id='2010'></a> Ludi et Antoine Begon, alors en stage chez Speculoos et OSP,
redessinent les fontes Crickx et les publient sur la fonderie OSP, ainsi
que sur d’autres plateformes de typographies libres. La Crickx commence
à circuler plus largement.
**2011**<a id='2011'></a> Arrivée de Sophie Boiron au studio Speculoos. Ouverture du
hackerspace Constant Variable à Schaerbeek, à 1 km de *Publi Fluor*.
Variable rassemble des pratiques artistiques inspirées par les logiciels
libres, et OSP y ouvre son premier studio. L’archive Crickx y est
déplacée et en constitue l’identité graphique. Chrystel le visite
pendant l’ouverture officielle et festive. L’archive est mise à
disposition du public.
**2013**<a id='2013'></a> Clôture de Variable, exposition d’une partie de l’archive Crickx
organisée pour l’occasion au centre culturel voisin, De Kriekelaar, puis
déplacement de l’archive dans les nouveaux bureaux d’OSP au 26<sup>e</sup> étage
d’une des trois tours WTC, Bruxelles.
**2014**<a id='2014'></a> Femke écrit *Today she started with C*, traduit aujourd’hui en
« Aujourd’hui, elle commence par le C ».
**2016**<a id='2016'></a> Spectacle non enregistré du magazine Médor (dont Ludi et Pierre
sont co-fondateur·ices) pour le Live Magazine au Théâtre National, avec
une courte conférence de Pierre sur l’« histoire Crickx ».
**05.04.2017**<a id='2017'></a> Festival Papier Carbone à Charleroi. Ludi et Stéphanie
Vilayphiou d’OSP découpent dans du vinyle des lettres Crickx « version
autotrace » pour les offrir au public.
**2017** À l’initiative de Sophie, rapatriement de l’archive vers les
nouveaux locaux de Spec uloos (renommé avec insertion d’une espace entre
les deux parties du nom), rue Van Elewyck 47 à Ixelles.
**2019**<a id='2019'></a> Décès du premier fils de Chrystel.
**10.2019** Début de l’exposition des matrices dans la vitrine de Spec uloos
à l’occasion de We Art XL.
**2020**<a id='2020'></a> Ouverture des fleuristes Nouveau à Ixelles, qui utilisent
uniquement la Crickx pour leur communication.
**2020** Un Futur pour la Culture, appel à projet lancé par la Fédération
Wallonie-Bruxelles (autre nom de la Communauté française). Les éditions
Surfaces Utiles y voient l’opportunité de financer la recherche qui
pourrait alimenter la publication d’un livre autour de l’archive Publi
Fluor. Fondation du Groupe de recherche Crickx à la suite de l’obtention
de la subvention.
**12.01.2021**<a id='2021'></a> Première réunion du Groupe de recherche Crickx.
**10.02.2021** Olivier Bertrand et David Le Simple lancent le Laboratoire de
recherche-création Crickx à l’ESA Le 75 autour de l’archive Publi Fluor
avec une équipe transdisciplinaire d’étudiant·es. Conférence inaugurale
de Pierre et Sophie, en tant que dépositaires de l’archive. 09--
**09-13.03.2021** Workshop « Déballage de l’archive » chez Spec uloos par les
étudiant·es et le groupe de recherche. Exposition de pièces de l’archive
dans la vitrine du studio et lettrage de celle-ci.

![]({static}/images/ecosystem/ecosystem_fig-5.jpg)

**31.03.2021** Pistage typographique du Laboratoire à la recherche de traces
de lettres Crickx dans le quartier de l’avenue Rogier, Schaerbeek.
**21.04.2021** Intervention de l’artiste-archiviste Mathieu Gargam dans le
Laboratoire de recherche-création.
**29.09.2021** Lancement du premier compte-rendu du Laboratoire de
recherche-création Crickx, rue Crickx à Saint-Gilles (il s’agit d’un
patronyme relativement courant à Bruxelles). Lettrage d’une vitrine pour
l’occasion.
**09.2021** Lancement de l’appel à contribution « Cherche amateurix de la
Crickx » pour collecter différents usages des fontes Crickx.
**20--21.10.2021** Touring Club à la recherche d’autres traces de lettres
dans la ville et interviews de quelques utilisateur·ices de la Crickx
par les étudiant·es du 75, David, Ludi, Olivier et Sophie.
**16.11.2021** Première interview-fleuve de Chrystel par le groupe de
recherche.
**20.04.2022**<a id='2022'></a> Restitution et présentation des projets de recherche-création
des étudiant·es du 75 en présence de Mathieu Gargam.
**07.05.2022** Workshop de clôture du Laboratoire de recherche-création en
présence du graphiste Axel Benassis, composition d’un spécimen à
l’échelle réelle des lettres tangibles de Chrystel Crickx pour un numéro
de la revue de typographie *La Perruque*. 09.2022 Nathan Izbicki, ancien
étudiant de l’ESA Le 75 et membre du laboratoire de recherche et
création, intègre le Groupe de recherche Crickx sur l’invitation de
celui-ci.
**20.11.2022** Deuxième interview-fleuve de Chrystel, rejointe par ses deux
sœurs.
**01.2023**<a id='2023'></a> Obtention d’une subvention de la Vlaamse Gemeenschap (Communauté
flamande) pour le projet de livre et les chantiers typographiques du
Groupe de recherche Crickx.
**06.03.2023** Conférence basée sur le texte « Mes Lettres », Femke défriche
publiquement les questions d’autorat et de licence liée à la Crickx dans
le cadre de la journée d’étude « Post\$cript, écoles sous licence » à
l’erg.
**04.2023** Obtention d’une aide à l’édition de la Fédération
Wallonie-Bruxelles pour le présent ouvrage.
**08-12.05.2023** Résidence du groupe de recherche au studio de Constant,
chaussée de Jette à Koekelberg.
**25-26.05.2023** Le groupe de recherche anime le workshop « Resharpening
Blobby » dans le cadre de l’Ultradependent Public School à BAK, Utrecht
(Pays-Bas).
**10.06.2023** Chrystel et le groupe de recherche se rendent en repérage aux
anciennes adresses des magasins et ateliers *Fluoréclam* et *Publi Fluor*
dans Schaerbeek.
**11-15.09.2023** Résidence du groupe de recherche à Meyboom Artist-Run
Spaces, Bruxelles.
**04.2024**<a id='2024'></a> Lancement à Schaerbeek de l’ouvrage *Publi Fluor*, affaires de
lettres à Bruxelles et republication de la police numérique sous le nom
*Publi Fluor*.


### Noms courants

#### Bruxelles 

Ville belge et capitale de la ­Belgique. Elle fait partie de la Région de
Bruxelles-Capitale, l’une des trois régions qui composent la Belgique,
elle-même composée de dix-neuf communes. Dans cet ouvrage on croise
notamment les communes de Schaerbeek, Anderlecht, Bruxelles-ville,
Forest, Molenbeek-Saint-Jean, Saint-Gilles, Ixelles et
Woluwe-Saint-Lambert. Dans le langage courant, le nom Bruxelles est
souvent utilisé à tort pour désigner l’ensemble de la Région
Bruxelles-Capitale.

#### ESA Le 75

Située dans la commune de Woluwe-Saint-Lambert, l’École Supérieure des
Arts de l’image Le 75 propose un bachelier artistique dans quatre
orientations : Graphisme, Images plurielles imprimées, Peinture et
Photographie.

#### Groupe de recherche Crickx

Publi Fluor est un projet mené par le Groupe de recherche Crickx,
composé de Sophie Boiron (typographe, graphiste, cartographe et
gardienne de l’archive Publi Fluor), Olivier Bertrand (éditeur,
designer, enseignant), Pierre Huyghebaert (designer, typographe,
cartographe, enseignant et gardien de l’archive Publi Fluor), Nathan
Izbicki (artiste, photographe), David Le Simple (libraire, éditeur et
enseignant), Ludivine Loiseau (typographe, designer et enseignante), et
Femke Snelting (chercheuse). La recherche aboutit au présent ouvrage
ainsi qu’au renommage et à l’extension du jeu de glyphes des versions
numériques de « la Crickx ».

#### Laboratoire de recherche-création Crickx

 Laboratoire de recherche et création constitué autour de l’archive
Publi Fluor par un groupe transdisciplinaire d’étudiant·es et
d’enseignants à l’ESA Le 75 entre fin 2020 et mai 2022. Membres : Soazig
Auvray, Camille Balseau, Olivier Bertrand, Pauline Barret, Leyla Cabaux,
Abigaël Coeffier, Léonard Gensane, Nathan Izbicki, David Le Simple,
Lysiane Schwab, Élise Tanguy.

#### Fréon

Fréon est une maison d’édition de bande dessinée contemporaine créée par
Thierry Van Hasselt, Vincent Fortemps, Olivier Poppe, Olivier Deprez et
Denis Deprez au début des années 1990 à partir de leur rencontre dans
l’atelier de bande dessinée de Saint-Luc à Bruxelles. Les livres édités
sont peu nombreux mais éditorialement exigeants et d’une grande richesse
plastique. En 2002, elle fusionne avec l’éditeur français Amok et
devient le FRMK (à prononcer Frémok).

#### Bye Bye Binary (BBB)

BBB est une collective franco- belge dont les actions alimentent le
débat sur la charge politique du design graphique, du langage, des
représentations des corps et des identités. BBB adopte une position de
recherche militante et communautaire, c’est-à-dire par et avec des
personnes concernées, à travers un prisme féministe, queer, trans\*,
pédé·e, bi·e, gouin·e. Depuis 2021 elle publie une typothèque qui
rassemble et diffuse une collection de caractères typographiques
post-binaires pour les usages du plus grand nombre.

#### Open Source Publishing (OSP)

OSP est un collectif de graphistes basé à Bruxelles. Leurs matériaux
sont la typographie, les sites web, les outils web-to-print et les
traceurs. 󲘐ls questionnent l’influence et les possibilités des outils
numériques à travers la pratique du graphisme (de commande), la
pédagogie et la recherche appliquée. À travers leurs projets et en
utilisant exclusivement des logiciels libres et Open source (F/LOSS),
i·els questionnent les logiciels en tant qu’objets culturels et les
modes de collaboration entre graphistes, artistes, institutions
culturelles et écoles.

#### Speculoos/Spec/Spec uloos

Spec uloos est un studio basé à Bruxelles. Depuis sa création en 2000,
par Pierre Huyghebaert, François Dispaux et Alexia de Visscher, les
lettres de son nom se déplacent. Il s’articule actuellement autour de
Pierre Huyghebaert et Sophie Boiron, avec la collaboration de plusieurs
indépendant·es. Œuvrant principalement dans le domaine culturel,
associatif ou public, Spec tisse des liens entre le réseau culturel
bruxellois, des acteur·ices de l’architecture et différentes
organisations liées aux questions de projets de ville, des maisons
d’édition, des organisations de théâtre et des artistes. Le bureau est
impliqué dans différents enjeux et recherches éclectiques mais connexes,
entre typographie, édition, co-conception d’expositions, conception de
signalétiques et cartographie, notamment au sein de la coopérative
Atelier cartographique.

#### Constant

Constant est une association sans but lucratif basée à Bruxelles depuis
1997 et active dans les domaines de l’art, des médias et de la
technologie. Constant travaille avec des serveures féministes, des
publications expérimentales, des archives actives, des réseaux
extitutionnels, des situations de (ré)apprentissage, des dispositifs
piratables, des protocoles performatifs, des infrastructures solidaires
et d’autres pratiques spongieuses pour tracer des voies vers des
technologies spéculatives, libres et intersectionnelles.

#### Variable

De 2011 à 2014, Constant investit un bâtiment appartenant à la
Communauté flamande à Schaerbeek et, sous le nom Variable, y abrite des
studios pour les artistes, les designers, les techno-inventeur·ices, les
activistes des données, les cyber-féministes, les geeks inter­actifs, les
pirates du textile, les vidéastes, les amateur·ices de son, les
beat-makers et d’autres créateur·ices numériques intéressé·es par
l’utilisation de logiciels libres et Open Source, dont OSP fait partie.

#### Halles de Schaerbeek

Centre culturel fondé par Philippe Grombeer et Jo Dekmine au début des
années 1970 dans une ancienne halle de marché aux légumes du XIX<sup>e</sup> siècle
à l’abandon. Acquise par la Fédération Wallonie-Bruxelles, ses
dimensions permettent d’accueillir des spectacles nécessitant de grands
volumes (Peter Brook, Fura dels Baus, Bob Wilson, Étienne Daho, Anne
Teresa De Keersmaeker ou même les premiers festivals Couleur Café dans
les années 1990). Centre Culturel européen de la Communauté Française
depuis 1991, les Halles font l’objet d’une rénovation en profondeur
avant de rouvrir en 1996. C’est à partir de ce moment et pendant trois
ans que Pierre Huyghebaert et Vincent Fortemps se chargent des
productions graphiques du lieu. En portant un projet de démocratie
culturelle, ses activités s’articulent autour des disciplines de la
danse, de la performance, du cirque et de la musique.





Title: Ecosystem
Date: 2024-12-04 10:20
Authors: Crickx research group
Summary: résumé
Slug: ecosysteme
Lang: en


### Key dates 

- [1910](#1910)
- [1916](#1916)
- [1939](#1939)
- [1943](#1943)
- [1944](#1944)
- [1953](#1953)
- [1957](#1957)
- [1958](#1958)
- [1961](#1961)
- [1961](#1965)
- [1968](#1968)
- [1969](#1969)
- [1970](#1970)
- [1972](#1972)
- [1973](#1973)
- [1974](#1974)
- [1980](#1980)
- [1984](#1984)
- [1985](#1985)
- [1989](#1989)
- [1991](#1991)
- [1994](#1994)
- [1996](#1996)
- [1997](#1997)
- [1998](#1998)
- [1999](#1999)
- [2000](#2000)
- [2001](#2001)
- [2003](#2003)
- [2004](#2004)
- [2006](#2006)
- [2007](#2007)
- [2010](#2010)
- [2011](#2011)
- [2013](#2013)
- [2014](#2014)
- [2016](#2016)
- [2017](#2017)
- [2019](#2019)
- [2020](#2020)
- [2021](#2021)
- [2022](#2022)
- [2023](#2023)
- [2024](#2024)



**1910**<a id='1910'></a> Birth of Raymond, Chrystel's father, in Tournai.
**1916**<a id='1916'></a> Birth of Alberte (nicknamed Betty), Chrystel's mother, in Ghent. 
**1939**<a id='1939'></a> Birth of Chrystel's brother.
**1943**<a id='1943'></a> Birth of Chrystel.
**1944**<a id='1944'></a> Birth of Marie-Claude, Chrystel's sister.
**1953**<a id='1953'></a> Raymond's first (work)shop, in a house on rue Vifquin, in Schaærbeek (Brussels), probably at number 10. The letterings he produces are based on hand-cut vinyl, while the vast majority of letterers work directly on their support with paint and brush.
**1957**<a id='1957'></a> The shop and the family move from Rue Vifquin to Rue des Coteau, at an unidentified number. 
**1957** Chrystel leaves school at age 14.
**1958**<a id='1958'></a> Birth of Marcelline, Chrystel's sister. The shop, under the name Fluoréclam, move to 80 Avenue Rogier \[Fig. 1\].

![]({static}/images/ecosystem/ecosystem_fig-1.jpg)

**1961**<a id='1961'></a> Chrystel's first marriage, at age 18. She lives in Ostend and works in a grocery store founded by her husband, then as an usher in a movie theater. She then opens a bar/dance club/bowling hall called 't Mandje. 
**1965**<a id='1965'></a> Chrystel, 22, leaves Ostend and her husband. She works at the Bonbons Antoine candy shop in Ixelles. Her artisanal qualities are noticed by the boss, who appoints her at the industrial design department
**1968**<a id='1968'></a> Chrystel meets the father of her first son. They live on Rue des Coteaux.
**1969**<a id='1969'></a> Birth of Chrystel's first son, Serge. The couple then break up. She meets and marries René Crickx.
**1970**<a id='1970'></a> Birth of Michel, Chrystel's second son. Roger Nols is elected
burgomaster of Schaærbeek. In this destitute municipality, he espouses a
populist, right-wing, anti-Flemish, and racist policy.
**1972**<a id='1972'></a> Raymond falls ill, and the three sisters work together in his
business for a couple of months.
**1973**<a id='1973'></a> Raymond dies. Chrystel takes over the shop and works with her
sisters. 

![]({static}/images/ecosystem/ecosystem_fig-2.jpg)

Their father's collages on the facade infuriate the building's owner, and Chrystel moves the business to number 74 of the same street. Her sister Marie-Claude opens a gift shop at number 72.
**1974**<a id='1974'></a> The Halles de Schaerbeek are converted into a cultural venue,
500 meters away from the shop.
**1974** Chrystel buys a house on 102 Avenue Rogier, and reopens the shop
under a new name, Publi Fluor. There, she cuts and sells individual
letters, but doesn't apply them for her customers.
**1980**<a id='1980'></a> The first computer-controlled cutting plotters are made
commercially available.
**1984**<a id='1984'></a> Following Xerox, the first graphical user interface computers are
made commercially available, with Apple, Microsoft, Adobe, and
PostScript. Gradually yet rapidly, independent professionals working
with drawing and letter composition adopt this new technology.
**1985**<a id='1985'></a> Without knowing their origins, Pierre Huyghebaert notices
Chrystel's letters on Schaærbeek shop windows, standing out amongst
other mostly-painted letterings.
**1989**<a id='1989'></a> Roger Nols' term as burgomaster ends. Belgium becomes a federated
state, and the Brussels-Capital Region is created.
**1991**<a id='1991'></a> Pierre starts producing experimental fonts with Misch Dimmer and
Karl Bassil during their studies at erg (école de recherche graphique,
Ixelles), under the moniker Hammerfonts.
**1994**<a id='1994'></a> After the Nols era, elected representatives of Moroccan and
Albanian origins enter the Schaerbeek municipal council.
**1996**<a id='1996'></a> After several years, the renovation of the Halles de Schaerbeek is
complete. Pierre now works in collaboration with artist Vincent
Fortemps. Together they design the center's graphic identity, producing
booklets and posters.

**1997**<a id='1997'></a> Working on posters for an Indian film festival, 

![pierre-huyghebaert-vincent-fortemps-new-sound-of-india-halles-de-schaerbeek]({static}/images/ecosystem/S-fig33.jpg)


Pierre
and Vincent consider using cut-out letters that adorn many windows
around the Halles, despite the progressive use of standardized types.
Tracing their origin, Pierre finds the Publi Fluor shop and meets
Chrystel. He orders a 4 cm high, 26 letters and 10 numbers, "normal
style" set (the most widespread italic), which he digitizes and
vectorizes with the Streamline software (afterwards integrated to Adobe
Illustrator). With these vectorized letters, he quickly sets the poster
title. Pierre then creates the first digital font, naming it "Crickx
Rush." It is used for other posters, flyers, programs and logos within
the Halles de Schaerbeek.

![pierre-huyghebaert-vincent-fortemps-new-sound-of-india-halles-de-schaerbeek]({static}/images/ecosystem/S-fig32.jpg)

**1998**<a id='1998'></a> Jan Middendorp, a journalist and editor of *Druk* type magazine,
published by FontShop Belgium, writes an article about Pierre and
Vincent's graphic production for the Halles. Pierre uses the font for
Fréon. He shares the type with other people in his professional circle
for practical reasons, without any clear specification in terms of
licensing.
**1999**<a id='1999'></a> Jan commissions Pierre for a 5-page article about Chrystel for
*Druk*. He writes it in French, designs the layout, and Jan translates
it into Dutch. When *Druk* issues a reader poll about its most popular
articles, Pierre's essay is ranked first. Chrystel closes her shop.
"I've done that \[cutting letters\] for 26 years." Between Christmas and
the New Year, René Crickx dies.
**2000**<a id='2000'></a> Chrystel retires and moves to a house in Wallonia.
**2001**<a id='2001'></a> Speculoos --- the studio of which Pierre is an associate with two
others --- buys all the remaining self-adhesive letters, as well as the
archives, for a symbolic price in order to prevent them from being
destroyed. At the end of the year, Pierre orders lower case letters,
punctuation marks and symbols from Chrystel in order to complete the
glyph set. She produces the version that would later be called "Blobby"
by OSP.
**2003**<a id='2003'></a> Crickx-rush-light-ext (one of the many optimized versions produced
by Speculoos for Flash websites), credited to Hammerfont, is leaked
across the Belgian-French contemporary comic-book publishing network and
is featured in the Paris-based magazine *Bang!*.

![bang-magazine]({static}/images/ecosystem/S-fig115.jpg)

**2004**<a id='2004'></a> Crickx letters are displayed in the Speculoos window on Chaussée de
Charleroi, in Saint-Gilles.
**2006**<a id='2006'></a> Harrisson, Femke Snelting, and Nicolas Malevé found Open Source
Publishing (OSP). The "caravan" is anchored in Constant's practice. Ludi
Loiseau arrives in Brussels through the École nationale des arts visuels
de La Cambre and meets Pierre, who has been teaching there for three
years. The second Print Party is organized by Harrisson and Femke at La
Quarantaine, in Ixelles. Pierre and Ludi are among the attendees. During
this event, an entire booklet is produced live, using various free
software. Ludi starts working for Speculoos. Pierre joins OSP for a
Print Party in Berlin with Harrisson as a substitute for Femke.
**2007**<a id='2007'></a> Ludi joins OSP during a trip to the Wrocaw Libre Graphics Meeting
(Poland), during which the first OSP fonts are designed. They are later
published under a SIL Open Font License (OFL).
**2010**<a id='2010'></a> Interning at Speculoos and OSP, Ludi and Antoine Begon redesign the
Crickx fonts and publish them on the OSP foundry as well as on other
free type platforms. Crickx starts circulating more widely.
**2011**<a id='2011'></a> Sophie Boiron joins Speculoos. The Constant Variable hackerspace
opens in Schaærbeek, 1 km away from Publi Fluor. Variable hosts art
practices inspired by free software, and OSP open their first studio in
the premises. The Crickx archive is relocated to the building and is
used for its graphic identity. Chrystel visits the place during the
official opening party. The archive is made available for the public.
**2013**<a id='2013'></a> Variable closes. A part of the Crickx archive is exhibited. The
event is organized at the neighboring cultural center, De Kriekelaar.
The archive is then relocated in OSP's new offices, on the 26<sup>th</sup> floor of
one of the two WTC towers in Brussels.
**2014**<a id='2014'></a> Femke writes "Today She Started With C".
**2016**<a id='2016'></a> The magazine Médor (co-founded by Ludi and Pierre) performs an
unrecorded show for Live Magazine at the Théatre national, with a short
lecture by Pierre about the "Crickx story."

**05.04.2017**<a id='2017'></a> Festival Papier Carbone, Charleroi: Ludi and OSP member
Stéphanie Vilayphiou cut an "autotrace version" of Crickx letters from
vinyl sheets, offering them to the public.
**2017** On Sophie's initiative, the archive is relocated in Spec uloos'
(renamed with spacing inserted between the two parts of the name) new
office space, 47 Rue Van Elewyck, Ixelles.
**2019**<a id='2019'></a> Chrystel's first son dies.
**10.2019** The matrices are displayed in Spec uloos' window during We Art
XL.
**2020**<a id='2020'></a> Florist Nouveau opens in Ixelles. They exclusively use Crickx for
their communication.

**2020** "Un Futur pour la Culture," a call for projects, is launched by the
Wallonia-Brussels Federation (a.k.a the Brussels French Community).
Surface Utiles editions grab this opportunity to fund a project that
could lead to a publication on the Publi Fluor archive. The Crickx
research group is founded after receiving the public grant.
**12.01.2021**<a id='2021'></a> First meeting of the Crickx research group.
**10.02.2021** Olivier Bertrand and David Le Simple launch the Laboratoire
de recherche-création Crickx at l'ESA Le 75, focused on the Publi Fluor
archive, alongside a transdisciplinary team of students. Pierre and
Sophie present an opening lecture as guardians of the archive.
**09-13.03.2021** The Publi Fluor unpacking workshop is organized at
Spec uloos by the students and the research group. Elements from the
archive are displayed in the studio's window, which is lettered for the
occasion.

![]({static}/images/ecosystem/ecosystem_fig-5.jpg)


**31.03.2021** The lab launches a typographic tracking, looking for Crickx
characters around Avenue Rogier, in Schaærbeek.
**21.04.2021** Artist/archivist Mathieu Gargam meets the lab as a guest.
**29.09.2021** Launch of the first report of the Laboratoire de
recherche-création Crickx, on Rue Crickx in Saint-Gilles (the name being
relatively common in Brussels). A window is lettered for the occasion.
**09.2021** An open-call for contributions titled "Cherche amateurix de la
Crickx" \["looking for Crickx lovers"\] is launched in order to document
the various uses of the font.
**20-21.10.2021** A Touring Club is organized to search for other traces of
the Crickx letters in Brussels. The students of Le 75, David, Ludi,
Olivier and Sophie interview several Crickx users.
**16.11.2021** Chrystel's first long-form interview is conducted by the
research group.
**20.04.2022**<a id='2022'></a> Restitution and presentation of the students' research and
creation projects, in the presence of Mathieu Gargam.
**07.05.2022** Closing workshop of the Laboratoire de recherche-création, in
the presence of graphic designer Axel Benassis. A 1:1 specimen of
Chrystel's letters is composed for an issue of the typographic journal
*La Perruque*.
**09.2022** Former Le 75 student and member of the lab, Nathan Izbicki, is
invited to join the Crickx research group.
**20.11.2022** A second long-form interview of Chrystel as well as her
sisters.
**01.2023**<a id='2023'></a> The Crickx research group receives a grant from the Vlaamse
Gemeenschap (Flemish Community) for their publication and typographic
research projects.
**06.03.2023** During a lecture based on her essay "My Letters" Femke
publicly unpacks the issues of authorship and of licensing linked to
Crickx during the "Post\$cript, écoles sous licence" symposium held at
erg.
**04.2023** The Wallonia-Brussels Federation delivers a grant supporting the
present publication. 08--12.05.2023 Residency of the research group at
Constant's studios, Chausée de Jette in Koekelberg.
**25-26.05.2023** The research group supervises "Resharpening Blobby" a
workshop organized during the Ultradependent Public School at BAK,
Utrecht (The Netherlands).
**10.06.2023** Chrystel and the research group visit the former addresses of
Fluoréclam et Publi Fluor in Schaerbeek.
**11-15.09.2023** Residency of the research group at Meyboom Artist-Run
Spaces, Brussels.
**04.2024**<a id='2024'></a> The book Publi Fluor. Letter Business in Brussels is launched in
Schaerbeek, and the digital font is republished as Publi Fluor.

### Common names

#### Brussels 

The capital city of Belgium. Part of the Brussels-Capital Region, one of
the three regions that make up the country of Belgium, itself composed
of nineteen municipalities. In this book are featured Schaerbeek,
Anderlecht, Bruxelles- Ville, Forest, Molenbeek-Saint-Jean,
Saint-Gilles, Ixelles and Woluwe-Saint-Lambert. In everyday language,
"Brussels" is often mistakenly used to refer to the Brussels-Capital
Region.

#### ESA Le 75 

Located in Woluwe-Saint-Lambert, the École supérieure des arts de
l'image Le 75 offers a Bachelor's Degree with four orientations: graphic
design, plural printed images, painting, and photography.

#### Crickx research group 

Publi Fluor is a project led by the Crickx research group, composed of
Sophie Boiron (type designer, graphic designer, cartographer, and
guardian of the Crickx archive), Pierre Huyghebaert (designer, type
designer, cartographer, and guardian of the Publi Fluor archive), Nathan
Izbicki (artist, photographer), David Le Simple (bookseller, publisher,
and educator), Ludivine Loiseau (type designer, designer, and educator)
and Femke Snelting (researcher). Their research has resulted in the
publication of this book, as well as in the renaming and extension of
the Crickx digital glyph set.

#### Laboratoire de recherche-création Crickx 

A research and creation laboratory formed around the Publi Fluor archive
by a transdisciplinary group of students and teachers from ESA Le 75
between late 2020 and May 2022. Members: Soazig Auvray, Camille Balseau,
Olivier Bertrand, Pauline Barret, Leyla Cabaux, Abigaël Coeffier,
Léonard Gensane, Nathan Izbicki, David Le Simple, Lysiane Schwab, Élise
Tanguy.

#### Fréon 

Fréon is a contemporary comic- book publishing house founded by Thierry
Van Hasselt, Vincent Fortemps, Olivier Poppe, Olivier Deprez, and Denis
Deprez in the early 1990s, after meeting at the comic-book section of
Saint-Luc, in Brussels. Their catalogue is small, but rigorous and rich.
In 2002, they merged with French publisher Amok, and became FRMK
(pronounced "Fremok").

#### Bye Bye Binary (BBB)

BBB is a French-Belgian collective whose actions nourish the debate on
the political dimension of graphic design, language, and of the
representation of bodies and identities. BBB lead their research with an
activist and communitarian stance, being operated by and working with
the concerned population, through a feminist, queer, trans, gay, bi, and
lesbian prism. Since 2021, they have published a type library gathering
and circulating a collection of post-binary characters intended for the
largest audience possible.

#### Open Source Publishing (OSP) 

OSP is a Brussels-based graphic design collective. Their primary
material is type design, websites, web-to-print tools, and plotters.
They question the influence and the possibilities of digital tools
through (commissioned) graphic design work, education and applied
research. Through their various projects that use free/libre open-source
(F/LOSS) software, they explore the notion of software as cultural
objects and the modes of collaboration between graphic designers,
artists, cultural institutions and schools.

#### Speculoos/Spec/Spec uloos 

Spec uloos is a Brussels-based studio. Since its inception in 2000, by
Pierre Huyghebaert, François Dispaux and Alexia de Visscher, the letters
of its name have been moving around. The studio is now run by Pierre
Huyghebaert and Sophie Boiron, in collaboration with several freelance
individuals. Mainly operating in the cultural, associative, and public
domains, Spec weaves connections between the Brussels cultural network,
actors in the field of architecture and various organizations focusing
on urban projects, publishing houses, theaters and artists. The office
conducts eclectic research projects linked to different yet connected
issues, between type design, publishing, exhibitions, signage and
cartographic design, especially within the Atelier Cartographique
cooperative.

#### Constant 

Constant is a non-profit organization based in Brussels since 1997,
active in the domains of art, media and technology. Constant works with
feminist servers, experimental publications, active archives,
extitutional networks, (re)learning situations, hackable apparatuses,
performative protocols, solidarity infrastructures and other porous
practices in order to open paths toward speculative, free and
intersectional technologies.

#### Variable 

Between 2011 and 2014, Constant was located in a building belonging to
the Flemish Community in Schaærbeek. Under the name Variable, it
sheltered studios for artists, designers, techno-inventors,
datactivists, cyberfeminists, interactive geeks, textile hackers,
videographers, sound enthusiasts, beatmakers and other digital creators
interested by the use of free/libre and open-source software, including
OSP. 

#### Halles de Schaærbeek

A cultural center founded by Philippe Grombeer and Jo Dekmine in the
early 1970s in a former vegetable market hall. Acquired by the
Wallonia-Brussels Federation, its dimensions allow to accommodate
large-scale shows (Peter Brook, Fura dels Baus, Bob Wilson, Étienne
Daho, Anne Teresa De Keersmaeker, and the first editions of the Couleur
Café festival in the 1990s). As a "European cultural center of the
French Community of Belgium" since 1991, the Halles underwent a
large-scale renovation before reopening in 1996. That same year, and for
three years, Pierre Huyghebaert and Vincent Fortemps were charged with
the graphic productions of the venue. Promoting the notion of cultural
democracy, the Halles' activities focus on dance, performance, circus
and music.

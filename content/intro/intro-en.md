Title: Intro
Date: 2024-12-05 10:20
Authors: Crickx research group
Summary: résumé
Slug: intro
Lang: en


Before the 2000s, the Schaærbeek shop windows were adorned with
vernacular letterings reflecting the different cultures living together
in this municipality, painted or cut and pasted by various
professionals. Among these signs, there was a recurring set of
letterforms that was certainly not machine-cut. Walking down Avenue
Rogier, passers-by could easily spot Publi Fluor, a shop where Chrystel
Crickx would cut them by hand. You couldn't miss it, as the shop was the
epicenter of a typographic wave that radially produced shop windows
during nearly forty years. 

When Chrystel Crickx closed down her business in 2001, graphic and type
designer Pierre Huyghebaert bought her entire stock of self-adhesive
letters in order to save them from destruction. Twenty years later, the
Crickx research group was formed around this archive, now kept by
Spec uloos (Sophie Boiron and Pierre). In parallel, a research and
creation lab was launched by the École Supérieure des Arts de l'image Le
75 by an interdisci-plinary team composed of teachers and students. A
riveting object of study, the archive aroused curiosity for the
eminently contemporary issues it raises: the economy of means (low-tech
production and alternative means of distribution), local practices
(craftspersonship, self-education, and vernacular, professional
amateurism), proximity and distance with the machine, property and
m·paternity.

In the present volume, the Crickx research group has documented the
archive and its various appropriations in order to publicize its
cultural and political stakes. The research on the Publi Fluor archive
is organized around points:

1: the history of Christel Crickx's business, her (work)shop and her
economy;

2: the design and the aesthetics of her vinyl letters. The shift from
materiality of digital fonts and their licenses. The contemporary uses
of Crickx. The family ties between every version of these letters; 

3: their traces in the city, transmission, variations.

Most of the methods and techniques used to understand these letters and
their ecology are unorthodox: unpacking the archive, partial
classification, first-person narrative, investigation, interviews,
mapmaking, appropriation, window lettering. The desire to play with
these letters mirrors their characteristics. They are brightly colored
(even fluorescent), modest (in relation with their creator's concern for
discretion), produced in large quantities (to meet the demand),
economical (the object of a viable business), vernacular (their traces
all over Brussels), eccentric (proposing an original model of slanted,
half-square, half-rounded letters), both transitory and sustainable
(vinyl being paradoxically as erosive as it is resistant), old-fashioned
yet vibrant (Publi Fluor is now closed down, but the digitization of
these letters has offered them an alternative mode of perpetuation) and
popular (drawn to be read by a large public, and supported by the
enthusiastic promotion of many members of the typographic community,
further encouraging the publication of this book).

Chrystel Crickx's letterforms seem to ignore typographic conventions.
Indeed, Chrystel had absolutely no training in the field. The eldest of
three sisters, she inherited her father's model — an ingenious letterer
— when she decided to take over his business upon his death. Unlike
him, who cut letters before applying them himself on his customers'
windows, Chrystel Crickx worked on her own and didn't want to step out
of her shop. She preferred selling her letters from inside,
progressively modifying her father's model in order to facilitate the
application of the letters for the non-specialists unaware of the joys
of typographic composition. Today, the research group and an entire
community of users call this method, which allowed Chrystel to produce
letters for the open air while staying inside her shop, the "Crickx"
model. Cutting letters in her kitchen behind the counter, she developed
a lettering practice that was less mobile, yet more commercial.

However, to what kinds of fields do these letters belong? Shop signs?
Lettering? Graphic design? Ornament? Utility? They actually seem to
inhabit the limits of all these categories. As a self-taught type
designer, Chrystel Crickx empirically devised inventive design solutions
that served her typographic and commercial practice. The Publi Fluor
archive is not only a letter repository, but also a registry of everyday
life, of the sources and of the economy of her unique practice. After
being hand-cut for local advertising uses, these letters have been
digitized and made more broadly accessible to users from all over the
world, working in other contexts. Inhabiting the margins of normalized
means of communication, they have contributed and still contribute to
the visual urban environment, in Brussels and elsewhere. 

This non-standard, collective attempt, made by the Crickx research group
and external contributors (practitioners, designers, active artists and
theorists) has tried to draw a portrait of a woman and her objects --
tools, letters, furniture and boxes -- while expanding the field of
investigation to examine the 'crackx' between the various histories
summoned by this practice. All of the keys necessary for your
exploration are supplied in the next chapter. Welcome to the Publi Fluor
ecosystem.
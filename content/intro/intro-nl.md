Title: Inleiding
Date: 2024-12-05 10:20
Authors: Crickx onderzoeksgroep
Summary: résumé
Slug: intro
Lang: nl

Tot in de jaren 2000 kon je op de winkelruiten in Schaarbeek
zelfgemaakte opschriften vinden, waaraan je de verschillende culturele
achtergronden kon aflezen van de mensen die de wijk bewoonden. Onder de
door diverse beletteraars en beletteraarsters geschilderde of geplakte
letters waren ook vernieuwende vormen die duidelijk niet machinaal
gesneden waren, en die hun weg vonden naar andere wijken in Brussel en
overal in België. Wanneer je over de Rogierlaan liep ontdekte je al snel
Publi Fluor, een winkel waar Chrystel Crickx die letters met de hand
uitknipte. De winkel was moeilijk te missen omdat ze het epicentrum
vormde van een trage, radiale typografische golf die zo'n veertig jaar
lang etalages kleurde.

Toen Chrystel Crickx in 2001 haar zaak sloot, kocht grafisch ontwerper
en typograaf Pierre Huyghebaert haar hele voorraad zelfklevende letters
op en voorkwam daarmee hun vernietiging. Twintig jaar later vormt zich
rond dit archief de Crickx onderzoeksgroep, een archief dat nu door
studio Spec uloos (Sophie Boiron en Pierre) wordt beheerd.
Tegelijkertijd start een interdisciplinair team van docenten en
studenten aan de École supérieure des Arts de l'image Le 75 het
'Laboratoire de recherche-création Crickx'. Ze zijn allemaal
nieuwsgierig naar het archief: een buitengewoon studie-object dat bij
uitstek hedendaagse kwesties aan de orde stelt: spaarzaamheid (low-tech
productie en alternatieve vormen van distributie); lokale praktijken
(vakmanschap, zelfstudie en een populair professioneel-amateuristische
praktijk); verwantschap met en vervreemding van machines; eigendom en
auteurschap. Met de bedoeling de culturele en politieke aspecten ervan
te ontsluiten, documenteert de Crickx onderzoeksgroep het Publi
Fluor-archief en de verschil-lende manieren waarop het werd toegeëigend
in deze publicatie. Het onderzoek rond het archief is onderverdeeld in
drie aspecten:

1.  de geschiedenis van het bedrijf van Chrystel Crickx, haar
    winkel-werkplaats en haar economie;

2.  het ontwerp van de vinylletters en hun esthetiek. De overgang van
    fysieke naar digitale lettertypes en de licenties van deze
    lettertypes. Hedendaags gebruik van Crickx. De relaties tussen alle
    versies van de letters;

3.  sporen in de stad, verspreiding en variaties.

De methoden en technieken die worden ingezet om de letters en hun
ecologie te begrijpen zijn weinig orthodox: het archief uitpakken,
gedeeltelijke ordeningen, persoonlijke verhalen, speurwerk, montage van
interviews, in kaart brengen, toe-eigenen, raambelettering. Het
verlangen om met de letters te spelen wordt uitgelokt door hun
kenmerken. Ze zijn fel (gemaakt van heldere, vaak fluorescerende
kleuren), bescheiden (vanwege de behoefte aan discretie van hun
maakster), in oplage geproduceerd (om aan de vraag te voldoen),
economisch (het onderwerp van een haalbare commerciele activiteit),
volks (de sporen zijn overal in Brussel terug te vinden), fantasievol
(een origineel voorstel voor scheve letters, half vierkant, half rond),
vergankelijk en duurzaam tegelijk (vinyl is paradoxaal genoeg even
vergankelijk als resistent), gedateerd maar springlevend (de winkel
Publi Fluor is gesloten, maar de digitalisering van deze letters
betekent dat ze op andere manieren blijven bestaan) en populair
(ontworpen om gelezen te worden door een breed publiek en gedragen door
het enthousiasme van heel wat mensen die deel uitmaken van de
typografische gemeenschap en die de publicatie van dit boek motiveren).

De typografische vormen van Chrystel Crickx lijken geen last te hebben
van disciplinaire stilistische conventies. Chrystel heeft ook geen
specifieke opleiding in die richting. De oudste van drie zussen, erft
zij de de werkwijze van haar vader, een inventieve beletteraar, wanneer
ze na zijn overlijden besluit zijn activiteiten voort te zetten. In
tegenstelling tot haar vader, die de letters uitknipte en ze dan op de
etalages van zijn klanten plaatste, werkt Chrystel Crickx alleen en is
ze er niet in geïnteresseerd om haar winkel te verlaten. In plaats
daarvan verkoopt ze de letters per stuk en past geleidelijk het model
van haar vader aan om het voor haar klanten, die het plezier van
typografisch componeren nog moeten ontdekken, gemakkelijker te maken om
zelf hun letters te plaatsen. Het is deze werkwijze die de
onderzoeksgroep en een hele gemeenschap van gebruikers nu 'La Crickx'
noemen. Het stelt haar in staat om letters voor buiten te maken, terwijl
ze binnen blijft. Door letters uit te snijden in de keuken achter haar
toonbank, ontwikkelt ze een minder mobiele maar commercieel handige
beletteringspraktijk.

Maar in welk domein horen deze letters eigenlijk thuis? Bewegwijzering?
Belettering? Grafisch ontwerp? Typografie? Ornamentiek?
Gebruiksvoorwerpen? Op de grens van alles, lijkt het. Autodidact
Chrystel Crickx vond op empirische wijze inventieve ontwerpoplossingen,
specifiek voor haar typografische en commerciële praktijk. Het Publi
Fluor-archief is daarmee niet enkel een depot voor letters, maar ook een
registratie van haar dagelijks leven, haar referenties en de economie
van haar unieke praktijk. Nadat de letters met de hand waren uitgesneden
voor lokale reclamedoeleinden, zijn ze gedigitaliseerd en breder
toegankelijk gemaakt voor gebruikers en gebruiksters van over de hele
wereld, in andere culturele contexten. In de marge van genormaliseerde
communicatiemiddelen hebben ze bijgedragen en blijven ze nog altijd
bijdragen aan de stedelijke visuele omgeving, in Brussel en elders.

Dit ongewone collectieve onderzoek van de Crickx onderzoeksgroep, met
bijdragen van externe medewerkers (makers en maaksters, ontwerpsters en
ontwerpers, kunstenaars en kunstenaressen, actieve theoreticae en
theoretici) probeert niet enkel een portret te zijn van een vrouw en van
haar objecten — gereedschappen, letters, meubels en opbergdozen — maar
verbreedt ook het veld om de breuklijnen te traceren tussen de
verschillende verhalen die deze praktijk oproept. Alle sleutels die je
nodig hebt om op onderzoek uit te gaan, vind je in het volgende
hoofdstuk. Welkom in het Publi Fluor-ecosysteem.

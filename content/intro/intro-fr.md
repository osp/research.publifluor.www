Title: Introduction
Date: 2024-12-05 10:20
Authors: Groupe de recherche Crickx
Summary: résumé
Slug: intro
Lang: fr

Avant les années 2000, les vitrines de Schaerbeek portaient des
lettrages vernaculaires reflétant les différentes cultures qui y
cohabitent, peintes ou découpées et collées à chaque magasin par des
lettreur·ses varié·es. Parmi elles, certaines formes inédites et
visiblement pas découpées par une machine revenaient avec constance, et
cela même un peu partout dans Bruxelles et en Belgique. En circulant
Avenue Rogier, on repérait très vite Publi Fluor, un magasin où
Chrystel Crickx les découpait à la main. Impossible de le rater, tant il
constituait l'épicentre de cette calme onde typo-graphique qui a
radialement produit des vitrines pendant une quarantaine d'années.

Lorsque Chrystel Crickx cesse ses activités en 2001, le graphiste et
typo­graphe Pierre Huyghebaert achète tout son stock de lettres
autocollantes afin d'en éviter la destruction. Vingt ans plus tard, le
Groupe de recherche Crickx se constitue autour de cette archive que
l'atelier Spec uloos (Sophie Boiron et Pierre) conserve aujourd'hui. En
parallèle, un laboratoire de recherche et création est aussi lancé à
l'École supérieure des Arts de l'image le 75 par une équipe
transdisciplinaire d'enseignants et d'étudiant·es. Formidable objet
d'étude, cette archive suscite la curiosité de toutes ces personnes pour
les problématiques éminemment contemporaines qu'elle soulève : économie
de moyens (production low-tech et moyens alternatifs de diffusion),
pratiques locales (artisanat, autodidaxie et amateurisme professionnel
vernaculaire), proximité et distance avec la machine, propriété et
m·paternité.

Dans le présent ouvrage, le Groupe de recherche Crickx documente cette
archive et les différentes appropriations qui en ont été faites, avec
l'objectif d'en rendre public les enjeux culturels et politiques. La
recherche autour de l'archive Publi Fluor s'est organisée autour de
trois chantiers :

1.  l'histoire de l'activité de Chrystel Crickx, sa boutique-atelier et
    son économie ;

2.  le dessin des lettres vinyles et leur esthétique. Le passage du
    tangible aux fontes numériques et les licences de ces fontes. Les
    utilisations contemporaines de la Crickx. Les filiations entre
    toutes les versions de ces lettres ;

3.  les traces dans la ville, la transmission, les variations.

Les méthodes et techniques qui ont été employées pour comprendre ces
lettres et leur écologie sont pour la plupart peu orthodoxes : déballage
de l'archive, classement partiel, narration à la première personne,
enquête, montage d'interviews, cartographie, appropriation, lettrage de
vitrines. L'envie de jouer avec ces lettres répond à leurs
caractéristiques. Elles sont lumineuses (faites de couleurs vives voire
fluorescentes), modestes (en relation avec le souci de discrétion de
leur autrice), produites en nombre (pour satisfaire la demande),
économiques (faisant l'objet d'un commerce viable), vernaculaires (leur
empreinte traversant Bruxelles), fantaisistes (proposant un modèle
original de lettres penchées, mi-carrées mi-arrondies), transitoires et
durable à la fois (le vinyle étant paradoxalement aussi érosif que
résistant), surannées mais vivantes (l'activité de la boutique
Publi Fluor a cessé, mais la numérisation de ces lettres leur permet de
perdurer autrement) et populaires (dessinées pour être lues d'un large
public et portées par l'enthousiasme d'un grand nombre de membres de la
communauté typographique qui motive la publication de cet ouvrage).

Les formes typographiques de Chrystel Crickx semblent affranchies des
conventions stylistiques de la discipline. Chrystel n'a en effet aucune
formation particulière en la matière. Entre elle et ses deux sœurs
cadettes, c'est elle qui hérite du modèle de leur père, lettreur
inventif, lorsqu'elle décide de prendre la suite de ses activités au
moment de son décès. Contrairement à celui-ci, qui découpait des lettres
avant d'aller les poser sur les vitrines de ses client·es, Chrystel
Crickx travaille seule et ne souhaite pas sortir de sa boutique pour
lettrer. Elle vend plutôt au détail et modifie progressivement le modèle
de son père pour faciliter la pose à ses client·es non-initié·es aux
joies de la composition typographique. C'est ce modèle que le groupe de
recherche et toute une communauté d'utilisateur·ices appelle aujourd'hui
« la Crickx ». Il lui permet de produire des lettres pour l'extérieur,
tout en restant à l'intérieur. En découpant des lettres dans la cuisine
à l'arrière de son comptoir, elle développe une pratique du lettrage
moins mobile mais davantage commerçante.

Mais à quels domaines ces lettres appartiennent-elles alors ? À
l'enseigne ? Au lettrage ? Au graphisme ? À la typo-graphie ? À
l'ornement ? À l'utilitaire ? Aux limites de tous ceux-ci semble-t-il.
Autodidacte, Chrystel Crickx trouve de manière empirique des solutions
de design inventives, propres à sa pratique typographico-commerçante.
L'archive Publi Fluor est non seulement un conservatoire de lettres,
mais aussi du quotidien, des sources et de l'économie de sa pratique
inédite. Après avoir été découpées à la main pour des usages
publicitaires locaux, ces lettres ont été numérisées et rendues plus
largement accessibles à des utilisateur·ices du monde entier dans
d'autres contextes. À la marge des moyens de communication normés, elles
ont contribué et contribuent toujours à l'environnement visuel urbain, à
Bruxelles et ailleurs.

Cet essai collectif non standard mené par le Groupe de recherche Crickx
et des contributeur·ices extérieur·es (praticien·nes, designeur·ses,
artistes et penseur·ses actif·ves) tente de dresser à la fois le
portrait d'une femme et de ses objets -- des outils, des lettres, des
meubles et des boîtes de rangements -- mais élargit aussi le champ pour
suivre les lézardes entre les différentes histoires que cette pratique
convoque. Toutes les clefs nécessaires à votre exploration vous sont
fournies dans le chapitre suivant. Bienvenue dans l'écosystème Publi
Fluor.
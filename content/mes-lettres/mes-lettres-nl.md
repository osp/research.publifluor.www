Title: Mijn letters
Date: 2024-12-03 10:20
Authors: Femke Snelting, Pierre Huyghebaert
Summary: résumé
Slug: mes-lettres
Lang: nl


De beweeglijke vinylletters die Chrystel Crickx maakte in de ruimte
achter haar winkel Publi Fluor in Schaarbeek zitten vol intieme gebaren:
de interactie van een extra fijn scheermes van Le Coq met de glibberige
weerstand van zelfklevend vinyl, het virtuoos hanteren van mes en
schaar, een voorliefde voor bepaalde vormen en niet voor andere, de
specificiteit van haar fysiek die de letters op haar manier maakt en de
ontspannen gewoonte om materiaal en tijd onder specifieke omstandigheden
te optimaliseren. 'Mijn letters', zoals Chrystel ze eenvoudigweg noemt,
zijn hoogst persoonlijk door de bijzondere verstrengeling van een
biografie, een lokale context en een tijdperk. Maar zoals deze
publicatie laat zien, doorkruisen de letters die bij Publi Fluor werden
verkocht nog veel meer lagen van productie en circulatie. Sommige van
die lagen opereren op lokale schaal, andere zijn industrieel of
infrastructureel van aard: de vinyl letters zelf, als economisch
waardevolle objecten in haar winkel en later als een cultureel archief,
een set sjablonen die aangepast en hertekend worden, de materiële
geschiedenis van vinyl, een verzameling gedigitali-\
seerde vectorobjecten die gevormd worden door een specifieke
software-esthetiek, variaties van een lettertype gepubliceerd als
'Crickx' uitgebracht onder een Open Licentie, of letters geplaatst door
haar klanten en ingezet door hedendaagse gebruikers. Samen vormen al
deze lagen een ecosysteem van fysieke en digitale intermediaire objecten
die bedoeld zijn om door anderen te worden geconfigureerd, geplaatst en
samengesteld. Deze dichte textuur van gebruik en hergebruik,
toe-eigening en circulatie roept heel wat vragen op over wat het
betekent om 'mijn letters' te zeggen. Door verschillende
ontstaans-verhalen te verkennen, reflecteert deze tekst op de manier
waarop eigendom en auteurschap door elkaar lopen voor wat we soms 'La
Crickx' noemen of elders 'het Publi Fluor-ecosysteem'.



### Gesjabloneerde gebaren

De begrippen origineel, kopie en vervolgens eigendom, auteurschap en
attributie fungeren telkens anders, afhankelijk van het onderdeel van La
Crickx dat je bekijkt. We beginnen deze tekst daarom met de verkenning
van het buitengewone Publi Fluor-ecosysteem. Zoals wordt uitgelegd in
'Twee generaties van de 3 vergeleken' en 'Verso⥀Recto', is het systeem
opgebouwd rond verschillende letterelementen die elk een serie
reproductieve relaties ondersteunen. De elementen maken het mogelijk om
vormen over te zetten van positief naar negatief, van sjabloon naar
individuele letters, en om ze te reproduceren in specifieke kleuren en
maten.

![De rondingen worden met de schaar uitgeknipt.]({static}/images/my-letters/1nl-ciseaux-cinq.png "De rondingen worden met de schaar uitgeknipt.")

Voor ons zijn de sjablonen de meest fascinerende objecten in het Publi
Fluor-ecosysteem. Ze bieden de materiële referentie voor de balpen die
de omtrek zal volgen van hun tijdelijke stabiele vorm, om zo een spoor
te markeren op de achterkant van zelfklevend vinyl. Elke vinylletter is
vakkundig met de hand gesneden, maar ze zijn niet helemaal uniek omdat
ze het spoor volgen van steeds hetzelfde sjabloon, dat op zichzelf weer
een reproductie is van andere letters, een combinatorische vorm. Omdat
de lettervormen iteratief zijn ontwikkeld door Chrystel en haar vader,
is het ook niet zo duidelijk of de sjablonen haar auteurschap nu juist
verwateren of bevestigen, of dat ze haar persoonlijke touch
kristalliseren en verspreiden. Door het repetitieve gebaar van het
steeds opnieuw overtrekken en uitsnijden van dezelfde vorm, drukt
Chrystel haar stempel op elk van de letters. Tegelijkertijd bevrijdt ze
ze op een of andere manier van een overdreven geaccentueerde signatuur.

Chrystel gebruikt de Franstalige term '*gabarit*' voor de sjablonen. De
term werd oorspronkelijk gebruikt in de context van scheepsbouw en
verwijst naar de halfronde stijve steunen waar met stoom planken omheen
worden worden gebogen, om zo een scheepsromp te vormen. Meer algemeen is
een *gabarit* een term voor voorwerpen met de fysieke eigenschap dat ze
de krachten kunnen weerstaan die nodig zijn om voorwerpen een precieze
vorm te geven, of die nu van hout, metaal, staal of een ander materiaal
gemaakt zijn. In een grafische context is een *gabarit* meestal een
negatief, gesneden uit een vel stijf en vaak transparant materiaal. De
gaten worden gebruikt om een mes, een mechanische pen of een plotter te
geleiden. De *gabarits* van Publi Fluor zijn positieve vormen. De pen
navigeert niet langs de binnenkant van de vorm, maar aan de buitenkant
en laat een markering op het vinyl achter die vervolgens wordt gevolgd
door een schaar of mes.

Zoals elke praktijk zijn eigen manier van reproduceren heeft en ook zijn
eigen standaardobject produceert, zou je kunnen zeggen dat de *gabarits*
'originelen' of 'bronnen' zijn in het Publi Fluor-systeem. Maar in
tegenstelling tot de metalen ponsen die werden gebruikt om klassieke
lettertypes te maken, worden deze *gabarits* niet bewaard in de reserves
van een prestigieuze gieterij of een nationaal museum. Ze hebben noch
materiële, noch monetaire waarde, niet alleen omdat de *gabarits* uit
een heel ander materiaal zijn gesneden, maar ook omdat ze identiek zijn
aan de lettervoorraad zelf. Als de juiste maat lastig te vinden is,
kunnen ze gemakkelijk opnieuw worden gemaakt door de vinyl letters
simpelweg over te trekken. Misschien zijn ze wel net zo vervangbaar als
een digitale vectortekening die steeds opnieuw in een bestand wordt
gekopieerd en geplakt.

![Progressieve variaties in het ontwerp wanneer je letters overbrengt naar sjablonen.]({static}/images/my-letters/2nl-r-empiles.png "Progressieve variaties in het ontwerp wanneer je letters overbrengt naar sjablonen.")


Na de verkoop van de letters en daarmee de overdracht van eigendom van
de individuele vinylletters aan haar klanten, wordt het beletteren zelf,
dat wil zeggen het in een bepaalde volgorde op een oppervlak plakken van
de letters, door anderen uitgevoerd volgens routines die buiten
Chrystels praktijk vallen. Het gevoel van auteurschap vervaagt, zoals
bij elke typografische overdracht, naarmate de letters meer in hun eigen
context beginnen te functioneren. In het Publi Fluor-ecosysteem opent
zich een ruimte voor extra spel en vibratie op het moment dat klanten de
teksten op hun ramen plakken. Ze kunnen de ruimte tussen de letters
aanpassen, hun horizontale uitlijning variëren en zelfs proberen ze
onvrijwillig te draaien -- delen van het proces die Chrystel niet kan en
wil controleren. Deze ontspannen en ongeremde relatie tussen origineel
en reproductie, waarbij ze haar klanten zelf laat bepalen hoe de letters
uiteindelijk op etalages, auto's en bewegwijzering terechtkomen, lijkt
centraal te staan in Chrystels praktijk. Het doet enigszins denken aan
de manier waarop Free Culture en Free Software gedijen door de digitale
kopie en digitale overvloed te omarmen. In tegenstelling tot in de
muziekindustrie, bijvoorbeeld, zijn digitale objecten in Free Culture
niet schaars en is het niet de bedoeling ze te willen bezitten: het
wordt als contraproductief beschouwd om de reproductie en circulatie
ervan te controleren of te proberen te beperken.


### Plakkerig eigendom

'Ah, ja, dit is mijn letters' (*Ah oui, ça c'est mes lettres*), zegt
Chrystel als we haar laten zien hoe de gedigitaliseerde versie van La
Crickx op verschillende manieren en in verschillende contexten wordt
ingezet.[^1] Maar van wie zijn deze gedigitaliseerde letters eigenlijk?
Wanneer we de digitale versies van La Crickx proberen toe te schrijven
aan een auteur, kunnen we op heel wat plekken beginnen.

We zouden bijvoorbeeld eind jaren 90 kunnen starten, wanneer de
studenten Pierre Huyghebaert, Karl Bassil en Misch Dimmer onder de naam
Hammerfonts een typografische praktijk ontwikkelen met een voorliefde
voor vernaculaire vormen.[^2] Het collectief ontwerpt lettertypes die
vaak gevonden voorwerpen als uitgangspunt hebben, zoals Alfphabet, een
lettertype dat is ontleend aan het Belgische snelweglandschap voordat
Hammerfonts een paar jaar later de officiële ontwerpen ontdekte.[^3]
Pierre en Karl dromen van een roadtrip om de snel verdwijnende
letterdiversiteit, die op dat moment nog steeds te vinden is op etalages
en openbare borden, te documenteren en te archiveren. Het plan is om
onderweg, al rijdend en tijdens stops op lokale dorpspleinen, instant
digitale lettertypes te produceren. Maar hoe te denken over het
auteurschap van de sporen die ze zeggen te willen bewaren? Wie is
bevoorrecht om vernaculaire vormen te documenteren, expliciet te maken,
een naam te geven en er een lettertype van te maken? Lettervormen, die
vaak door anonieme letteraars of letteraars waarvan we de naam niet
kennen zijn gemaakt, worden maar al te vaak toegeschreven aan hun
archiefbewaarders, vooral zodra er gedigitaliseerde versies beginnen te
circuleren nadat de vormen zorgvuldig zijn gefotografeerd,
gevectoriseerd en gespatieerd. De droom van wat de drie destijds een
'typo-safari' noemden komt uiteindelijk nooit uit (of in ieder geval
niet op deze schaal), maar we kunnen ons voorstellen hoe het verzamelen
en vervolgens hergebruiken van een verdwijnende letterkunst zowel een
daad van zorg als een daad van toe-eigening zou zijn.

Het verhaal kan ook beginnen op het moment dat Pierre 'La Crickx'
opmerkt in de straten van Schaarbeek, de letters herleidt naar de
Publi Fluor-winkel, en vervolgens een flinke set in verschillende maten
en kleuren koopt.[^4] De gescande en gevectoriseerde letters zijn nog
geen lettertype, maar eerder een digitale map gevuld met onafgemaakte
digitale objecten. Om een tekst te kunnen samenstellen moeten ze iedere
keer opnieuw handmatig worden gedupliceerd, uitgelijnd en gespatieerd in
een vectorprogramma. Het werken met zulke onderontwikkelde proto-fonts
is een veel voorkomende digitale praktijk onder grafisch ontwerpers die
zich voor het eerst aan typografie wagen. Omdat de letters nog steeds
werken als afzonderlijke objecten, blijft de praktijk dicht bij de
manier waarop klanten van Publi Fluor individuele letters handmatig op
hun etalages plaatsten. 

Toen Pierre een set letters bij Chrystel kocht, kocht hij toen ook het
recht om ze digitaal te reproduceren? In juridische termen
waarschijnlijk wel. Het auteursrechtelijk kader voor digitale
lettertypes is gebaseerd op de aanname dat ze 'te nuttig zijn om te
beschermen'. Zoals Eric Schrijver uitlegt in *Copy This Book*, 'maakte
hun traditionele link met technologie het niet vanzelfsprekend dat hun
bescherming op dezelfde manier zou werken voor lettertypes als voor
beeldende kunst en schrijven'.[^5] Auteursrecht werkt min of meer op
dezelfde manier voor een lettertype als voor software broncode, wat
betekent dat alleen de codering als lettertype beschermd is, maar hun
specifieke ontwerp of verschijningsvorm niet. Wettelijk gezien heeft
iedereen dus het recht om deze letters te digitaliseren, maar in
ethische termen ligt de situatie misschien iets anders. Pierre verbergt
niet voor Chrystel dat hij de gekochte letters gaat inscannen, maar
omdat haar praktijk op geen enkele manier digitaal is, heeft ze de
impact daarvan misschien niet direct begrepen.[^6] Ze weigert ook niet
om Pierre's vragen te beantwoorden, wetende dat hij een artikel over
haar werk schrijft. Ze heeft er duidelijk plezier in om het onderwerp
van een publicatie te zijn, maar is het haar volledig duidelijk dat de
aandacht haar misschien meer identificeerbaar zal maken en dat dit een
inbreuk op haar privacy zou kunnen betekenen?[^7]

Om in-software tekstcompositie mogelijk te maken, voltooit Pierre de
vectorpaden en zet ze om in een digitaal lettertype. Hij geeft het
lettertype tijdelijk de naam 'Crickx Rush' om aan te geven dat er
duidelijk nog meer werk te doen is, maar misschien ook als verwijzing
naar zijn gemoedstoestand in de voortdurende stroom van projecten die
het moeilijk maken om aan zijn precaire economische omstandigheden te
ontsnappen. In deze versie werkt de geautomatiseerde vectorisatie
opmerkelijk goed samen met de scherpe randen van de met de hand gesneden
letters, en dat genereert verrassende hoeken, knikken en randen. Hij
kiest ervoor om de letters op min of meer dezelfde basislijn te plaatsen
en besteedt niet te veel tijd aan het aanpassen van de ruimte tussen de
letters. La Crickx Rush is gebaseerd op scans van letters die op
verschillend formaat zijn gesneden, variërend tussen 3 en 30 cm. Het is
ook een mix van uiteenlopende stijlen omdat de set die Pierre
oorspronkelijk kocht verschillende iteratieve aanpassingen bevat:
letters die waarschijnlijk door Chrystels vader zijn gesneden, en andere
door Chrystel zelf. Dit hybride digitale object circuleert nu onder
Crickx, de naam die Chrystel van haar echtgenoot kreeg na hun huwelijk.
Informeel wordt het lettertype soms toegeschreven aan Pierre of aan
Hammerfonts.[^8]


![Ontmoeting met Chrystel om het ontwerp van de kleine letters te bespreken.]({static}/images/my-letters/3nl-commande-blobby.jpg "Ontmoeting met Chrystel om het ontwerp van de kleine letters te bespreken.")


Een andere rode draad verschuift de kwestie van eigendom, auteurschap en
toeschrijving opnieuw. Omdat Chrystels voorraad enkel uit kapitalen
bestaat, bezoekt Pierre de inmiddels gepensioneerde lettermaakster in
haar nieuwe thuis op het Waalse platteland en geeft haar de opdracht een
set kleine letters te ontwerpen. Hij is bezorgd dat de bijzondere vormen
van de Gill Sans, een lettertype dat hij gebruikt om het ontwerp van
tekens en symbolen[^9] over te brengen, sommige daarvan onbekend bij
Chrystel, haar teveel zullen beïnvloeden in haar ontwerpkeuzes. Zijn
zorgen blijken ongegrond als hij een paar weken later de gesneden
vinylletters ontvangt, want de letteraarster heeft een onverwachte route
genomen. Het alfabet dat OSP later 'Blobby' zal noemen, is uniek. De
fantasierijke of zelfs psychedelische lettervormen hebben geen directe
relatie met de hoofdletters in La Crickx en ze vormen samen dan ook geen
coherent lettertype. Chrystelise (de huidige naam van Blobby) is een
interessante anomalie in het Publi Fluor ecosysteem omdat het, in
tegenstelling tot andere elementen, in één keer is ontwikkeld als een
set. Dit plaatst Chrystelise dichter bij de praktijk van typografie, wat
ook betekent dat het misschien meer voor de hand ligt om dit werk aan
haar persoon toe te schrijven.


### Digitale attributie

Wanneer Chrystel met pensioen gaat, koopt Pierre voor een bedrag van
10.000 Belgische frank (nu ongeveer 1500 euro) de resterende voorraad
van Publi Fluor op, inclusief een deel van het opslagmeubilair, de
sjablonen en een gedeelte van het administratieve archief. Een paar jaar
later ontmoet hij Ludi Loiseau en ze beginnen samen te werken bij
Speculoos. Ze krijgen gezelschap van Antoine Begon en alle drie sluiten
ze zich uiteindelijk aan bij Open Source Publishing (OSP). Hun
affiniteit met het verhaal van Chrystel en met het Publi
Fluor-ecosysteem motiveert hen om de vinyl letters, met iets minder
haast, opnieuw te digitaliseren. Ludi en Antoine besluiten Chrystelise's
verrassende onderkast mee te nemen en de vectorsporen van elke letter
opnieuw te tekenen, terwijl ze waar nodig opnieuw typografische keuzes
maken.

Wanneer het lettertype, dat nu verschillende versies van Crickx bevat,
opnieuw klaar is om gepubliceerd te worden, lijkt het voor de hand te
liggen om dat te doen onder de condities van een Open Font License
(OFL). We hebben het gevoel dat Crickx, nu nog meer een hybride object
geworden, er baat bij kan hebben om te circuleren onder de genereuze
voorwaarden die deze licentie biedt. OFL maakt deel uit van het soort
paralegale interventies die gegroepeerd kunnen worden onder Open
Content, copyleft of Free Culture licenties. Door gebruikers expliciet
toe te staan het lettertype te bestuderen, te verbeteren, te verspreiden
en te kopiëren, buigen deze licenties het conventionele auteursrecht om
andere manieren van delen mogelijk te maken binnen het kader van de
wet.[^10] Misschien nog belangrijker is dat Free Culture licenties een
aanzet geven om auteurschap te zien als een genetwerkte relationele
praktijk, of zoals OSP in 2011 stelde: 'Ons enthousiasme voor Vrije
Software komt voort uit de conceptie zelf, omdat het gebaseerd is op een
collectieve praktijk die een netwerk van relaties creëert tussen
specifieke gemeenschappen, gereedschappen en praktijken.'[^11]

Als je tien jaar later de credits leest die zijn opgeslagen in het
font-log, vind je verrassend genoeg de onverbloemde vermelding
'Copyright (C) 2011 OSP'.[^12] Over de inmenging van OSP in de
auteursgenealogie van deze letters en de claim van auteurschap over de
gedigitaliseerde versie van La Crickx is destijds niet veel nagedacht,
maar die keuzes roepen nu veel vragen op.[^13] Waarom voelde OSP zich in
de eerste plaats geroepen om deze lettervormen te publiceren, en wat gaf
hen het recht om dit te doen onder een specifieke licentie?[^14] Hoe
kunnen we de rol van vinyl, het scheermes, het algoritme dat de digitale
randen definieerde, de Brusselse esthetiek die de vormen meebepaalde,
maar ook het niet-aflatende enthousiasme van de 'gebruickxers' een plek
geven? Hoe beïnvloeden hun interpretaties het lettertype en
co-construeren ze het toekomstige hergebruik ervan? Had OSP gelijk om
aan te nemen dat La Crickx op een of andere manier al een gedeelde bron
was, of hebben ze het auteurschap van Chrystel uitgewist door zichzelf
te laten gelden? De keuze om dit lettertype 'La Crickx' te noemen was
een manier om een lokale ambachtsvrouw en haar gesitueerde letters in te
schrijven in een lijst van typografische helden, ergens na John
Baskerville en voor Adrian Frutiger. Maar het zorgde er ook voor dat
Chrystel ten onrechte een auteur onder hen werd doordat het precieze,
uitgewerkte ecosysteem dat Publi Fluor met La Crickx tot stand bracht,
ten onrechte tot typografie werd teruggebracht.

Als OSP samen met Constant naar Variable verhuist, een collectieve
werkruimte voor Free, Libre en Open Source artistieke praktijken,
slechts een paar straten verwijderd van waar Publi Fluor oorspronkelijk
gevestigd was, verhuist de lettervoorraad op hun verzoek ook terug naar
Schaarbeek. Het enthousiasme voor de felgekleurde lettervormen wordt
snel overgenomen door alle leden van de Variable-gemeenschap en Chrystel
accepteert de uitnodiging van OSP om als eregast aanwezig te zijn bij de
opening van de collectieve werkruimte en de heruitgave van La Crickx.
Terwijl hun associatie met het digitale lettertype en het verhaal van
Chrystel het culturele kapitaal van OSP heeft vergroot, is het nog niet
zo duidelijk wat de publicatie voor Chrystel betekent, behalve alweer
een gebaar van zowel zorg als toe-eigening.

Deze articulatie van de wederzijdse erkenning, validatie en vermenging
van auteurschap die plaatsvond en nog steeds plaatsvindt rond het
digitaliseren van het Publi Fluor-ecosysteem heeft even op zich laten
wachten. Het digitaliseren van La Crickx was steeds opnieuw een poging
om de letters uit hun fysieke archieven te bevrijden en los te maken van
hun afhankelijkheid van het hyperlokale; misschien heeft het de
verdwijning van de vormen uit het Brusselse landschap kunnen voorkomen.
De circulatie van de letters viert de relatie tussen verschillende
soorten gebruik en werk, en Free Culture biedt ons een kader om dat te
doen. Maar om de sedimentatie van conventionele percepties van
auteurschap en auteurschap en eigendom, is een copyleft licentie niet
genoeg. We moeten vragen blijven stellen over voorwaarden voor gebruik
en hergebruik, over wat telt als oorsprong en als kopie; over wat eerst
komt en wat altijd als laatste.

### De kost verdienen

In een tekst uit 1996 beschrijft de feministische auteur Kathy Acker de
complexe relatie die ze als marxiste heeft met het auteursrecht: 'Als
schrijvers zijn we economisch afhankelijk van het auteursrecht, van het
bestaan ervan, omdat we leven en werken, of we dat nu leuk vinden of
niet, in een burgerlijk-industriële maatschappij, in een kapitalistische
maatschappij, een maatschappij gebaseerd op eigendom. In feite moeten we
bezitten om te overleven, om te bestaan.'[^15] In de context van een
markteconomie is de intellectuele expressie van een auteur gewoon een
object dat je net als ieder ander kan bezitten, handelswaar die kan
worden verkocht en geëxploiteerd. Voor Acker waren de transacties
waarmee ze haar huur kon betalen gebaseerd op diep autobiografische
teksten waarvoor ze werd betaald via royalty's en publicatiecontracten
of die ze in opdracht had geschreven. Leven van je creatieve werk in een
kapitalistisch systeem produceert paradoxale interafhankelijkheden,
waarbij overleven en bestaan op een maar al te bekende manier in elkaar
overlopen.

Voor degenen die professioneel betrokken zijn bij Free Culture zijn deze
paradoxen niet verdwenen. De praktijk van Free, Libre en Open Source is
gebaseerd op de aanname dat digitale kopieën niet verschillen van hun
zogenaamde originelen. In essentie kan en mag een digitaal bestand
daarom niet onderhevig zijn aan exclusief eigendom. Dit betekent dat als
je geld wil verdienen met Vrije Cultuur, je een manier moet vinden om
diensten in rekening te brengen in plaats van objecten te verkopen,
zoals een speciale toepassing of ontwikkeling op maat. In de neoliberale
omgeving van culturele productie valt dit samen met het groeiende belang
van cultureel kapitaal en tegelijkertijd met de waarde van herkenbaar
auteurschap. Het geraffineerde beheer van een signatuur of
merkidentiteit vervangt een economie die gebaseerd is op auteurschap als
eigendom.

Voor de meeste letterontwerpers is het 'businessmodel' dat commerciële
digitale uitgevers van typografie toepassen niet relevant. Digitale
lettertypes moeten nu eenmaal gekopieerd worden om hun werk te doen en
alleen grote lettergieterijen kunnen kunstmatige schaarste bereiken door
een leger advocaten te combineren met een cultuur van angst. De
beslissing om La Crickx te publiceren onder voorwaarden die anderen in
staat stellen het lettertype vrij te gebruiken heeft daarom niet enkel
een politieke betekenis en goede bedoelingen, maar volgt in zekere zin
ook een commerciële logica. De projecten rond La Crickx laten weliswaar
niemand van ons (OSP, haar utilisatrickx noch de leden van de
onderzoeksgroep)[^16] toe om in zijn of haar levensonderhoud voorzien,
maar onze inzet voor het voortbestaan van La Crickx als een digitaal
lettertype heeft wel degelijk cultureel kapitaal gecreëerd, wat
aanleiding was tot uitnodigingen voor workshops en presentaties. Het
leverde uiteindelijk dit onderzoeksproject op dat werd gefinancierd met
twee culturele subsidies.

![Workshop in BAK, ook een manier om je brood te verdienen. ]({static}/images/my-letters/4nl-bak-IMG_20230526_181729.jpg "Ontmoeting met Chrystel om het ontwerp van de kleine letters te bespreken.")


Terwijl de gedigitaliseerde versie van La Crickx circuleert onder de
genereuze voorwaarden van de OFL, blijven de fysieke vinyl objecten
collega's, studenten en anderen enthousiast maken. In Variable neemt OSP
de rol van beheerder van het Publi Fluor-archief op zich, en later,
wanneer Variable zijn deuren sluit en de voorraad en het opslagmeubilair
weer verhuizen, nu naar de kantoren van Speculoos in Elsene, nemen
Sophie Boiron en Pierre die rol op zich. Na de pensionering van Chrystel
zijn de vinylletters getransformeerd van een continu bijgevulde voorraad
commerciële activa in een eindige verzameling culturele curiosa, zo niet
cultureel erfgoed. We vinden het belangrijk dat ook de fysieke letters
in de straten van Brussel blijven circuleren, maar omdat er geen nieuwe
letters meer worden gesneden, zijn we ons er terdege van bewust dat de
voorraad langzaamaan uitgeput zal raken. Pierre en Sophie ontwikkelen
een praktijk van selectieve en strategische vrijgevigheid, waarbij de
vinyl letters spaarzaam af en toe worden weggegeven. Als ze Chrystel
hierover vertellen, is ze geschokt. Hoe kunnen deze voorwerpen, die haar
levensonderhoud vertegenwoordigden, gratis worden weggegeven?[^17]

Er zijn merkwaardig veel verschillen tussen de praktijk van Publi Fluor
en de onze. De klanten van Publi Fluor lijken het bijvoorbeeld niet erg
te vinden dat hun uithangborden de herkenbare signatuur van Chrystel
dragen, in resonantie met het Brusselse stadslandschap in plaats van hun
eigen specifieke merk. Maar waarderen ze haar stijl en expertise
eigenlijk wel, of zouden ze de voorkeur geven aan machinaal gesneden
belettering, als ze zich dat hadden kunnen veroorloven? Bij het
doornemen van Chrystels handgeschreven administratie, geschreven op de
achterkant van offertes en andere zakelijke correspondentie, proberen we
ons een voorstelling te maken van de dagelijkse werkzaamheden van Publi
Fluor en hoe ze als handelsvrouw zou hebben besloten hoeveel werk ze
precies aan kon nemen, hoeveel voorraad ze moest aanleggen en hoeveel
letters ze per dag, per week of in een mensenleven moest verkopen om een
comfortabele economische situatie voor zichzelf en haar gezin te
creëren. Het zou kunnen zijn dat haar specifieke economische benadering,
die het resultaat is van het pragmatisme op meerdere schaalniveaus dat
komt kijken bij het creëren van een bestaan op basis van letters, de
kwaliteit van het Crickx ecosysteem bepaalt. De manier waarop de letters
zijn geoptimaliseerd voor een minimum aan speculatie en ambitie is
zeldzaam en heeft een grote impact op hun esthetiek. Het bepaalt hun
kwaliteit en de aantrek-kingskracht die ze op ons hebben.

Met de uitspraak 'Dit is mijn letters' (*C'est mes lettres*) drukt
Chrystel een gevoel van eigendom uit dat geen auteursrechtelijke of
artistieke claim lijkt te zijn. Haar 'mijn' (*mes*) verwijst eerder naar
een gevoel van vertrouwdheid, in de zin van 'door mij gemaakt, in mijn
tijd'. Het gebruik van het enkelvoud 'dit is' (*c'est*) is
waarschijnlijk een Brusselse manier van spreken, maar zou ook een
praktijk kunnen aanduiden, in plaats van de individuele objecten. 'Dit
is mijn letters' legt zo een directe relatie tussen haar persoonlijke
werk, haar leven en de herkenbare objecten in de wereld om haar heen.

### Een overtuiging om te delen

Het Publi Fluor onderzoeksproject werd deels ingegeven door een groeiend
onbehagen over de manier waarop het benoemen, licenseren en
contextualiseren van het digitale lettertype onder de noemer Vrije
Cultuur het collectieve karakter van het Publi Fluor-ecosysteem uit het
oog leek te verliezen. In onze dagelijkse praktijk proberen we ons te
verzetten tegen het dominante ideologische kader van conventioneel
intellectueel eigendom en gestandaardiseerde aannames over individueel
auteurschap, exclusiviteit en originaliteit. Dit valt samen met een
diepe interesse in de praktijk van Free, Libre en Open Source software
en Vrije Cultuur, die ons werk al jarenlang energie geven. We moesten
ons daarom afvragen hoe we in ons engagement met Publi Fluor verder
konden gaan dan het vastleggen van de individuele biografieën en
praktijken rond La Crickx, vooral omdat de juridische techniek van
licentiëring altijd eerst begint met het vaststellen van een wettelijke
auteur. We vroegen ons af hoe we in plaats daarvan het
Publi Fluor-ecosysteem konden behandelen als een al collectief
ecosysteem van productie, dat is gemaakt om te worden gebruikt,
uitgebreid en opnieuw in circulatie te worden gebracht. De Mexicaanse
auteur Cristina Riviera Garza stelt disappropriatie voor als een
creatieve praktijk die weerstand biedt aan het kapitalistische systeem
dat arbeid en leven definieert. Als een praktijk van zorgvuldig
toe-eigenen is disappropriatie een collectieve manier van schrijven,
maken en creëren met het werk van anderen; een manier om 'de pluraliteit
die voorafgaat aan individualiteit in het creatieve proces, bloot te
leggen en een venster te openen op de materiële gelaagdheid die zo vaak
verborgen wordt gehouden door toe-eigenende teksten.' Het lijkt erop dat
de verschillende constellaties van mensen die zich al meer dan twee
decennia inzetten voor deze mobiele vinyl letters zo'n disappropriatieve
stap hebben gezet door zich te verbinden aan een praktijk die de
pluraliteit van het Publi Fluor-ecosysteem mee vormgeeft, articuleert en
deelt.

![Tests voor post-binaire labels, voor een aftiteling in het Frans.]({static}/images/my-letters/monteurse-directeurice.png "Tests voor post-binaire labels, voor een aftiteling in het Frans.")


Deze publicatie gaat gepaard met een vierde of vijfde heruitgave van de
digitale lettertypes die we tot nu toe La Crickx noemden. De
lettervormen zijn bijgewerkt en opnieuw op elkaar afgestemd, en bevatten
nu een set nieuwe glyphs die typografisch vorm geven aan de dringende
behoefte aan genderinclusieve taal. We hebben het font-log herschreven
als antwoord op een aantal van de vragen die in deze tekst zijn
onderzocht. We hebben de collectie lettertypes ook omgedoopt tot
Publi Fluor, in een poging om het zwaartepunt te verleggen van één
persoon naar een activiteit, een praktijk. Deze hernoeming kan
natuurlijk gelezen worden als het schrappen van een hommage, een
uitwissing, want natuurlijk maakt het uit wie er componeert, schrijft,
codeert, tekent of knipt. Maar zoals Sara Ahmed schrijft, is er spanning
tussen erkenning en individualisering, omdat 'een feministische
benadering het zich niet kan veroorloven om vragen over belichaming en
subjectiviteit te herleiden tot de ontologie van identiteit'. Met andere
woorden, we kunnen er niet van uitgaan dat de belichaming van de auteur
naadloos aansluit bij haar identiteit, noch bij het werk zelf. We
nodigen je daarom uit om je dit boek voor te stellen als een uitgebreid
specimen, dat niet alleen de buitengewone esthetische kwaliteiten van
een lokale letterpraktijk laat zien, maar ook de nadruk legt op de
sociaaleconomische context van het Publi fluor-ecosysteem, in plaats van
te proberen het genie van een enkele bedenkster te vieren. De ervaring
om de specificiteit van Chrystels praktijk onder ogen te zien terwijl we
ons haar letters eigen maken, doorkruist veel verschillende opvattingen
over wat het betekent om 'mijn letters' te zeggen. Het Publi Fluor
ecosysteem daagt ons uit om minder vooropgestelde richtingen in te slaan
dan de conventionele typografische, grafische, software- en industriële
praktijken gewoonlijk toestaan.


[^1]: Interview met Chrystel Crickx, november 2021.

[^2]: Voor meer details over de overlappingen tussen Hammerfonts,
    Variable, OSP en Speculoos, zie 'Publi Fluor Ecosysteem'.

[^3]: Open Source Publishing, 'Alphabet', OSP Foundry, september 2013.
    osp.kitchen/foundry/alfphabet/

[^4]: Pierre zag de letters in de etalages van Schaarbeek vanaf zijn
    brommer, tijdens zijn eerste tochten naar de hoofdstad in zijn late
    tienerjaren halverwege de jaren 80. In het midden van de jaren 90,
    toen Pierre en Vincent Fortemps aan projecten werkten voor de Hallen
    van Schaarbeek, spraken ze over die letters en vond Pierre de
    winkel.

[^5]: Eric Schrijver, *Copy This Book: An Artist's Guide to Copyright*
    (Eindhoven: Onomatopee, 2019), 123.

[^6]: Pierre herinnert zich een discussie met Chrystel toen hij haar de
    New Sound of India poster overhandigde. Dat de poster was beletterd
    met bijvoorbeeld drie keer dezelfde letter N, terwijl Pierre er maar
    één had gekocht. En hoe de lettergroottes op de poster waren gemaakt
    op een manier die niets te maken had met de lettergroottes die in de
    winkel worden verkocht. Chrystel reageerde op Pierre's pogingen tot
    een semi-technische uitleg met een zekere berusting: 'Maar goed, ik
    weet niet eens hoe een fax werkt.'

[^7]: In de gesprekken met Chrystel tijdens het redigeren van dit boek
    kwam haar verlangen naar privacy meerdere keren ter sprake, met een
    expliciet verzoek van Chrystel om geen informatie op te nemen die
    haar huidige woonadres zou onthullen

[^8]: Rond 2003 is het lettertype crickx-rush-light-ext, een van de vele
    geoptimaliseerde versies die Speculoos produceerde voor gebruik in
    Flash-websites (toegeschreven aan Hammerfonts), via het
    Belgisch-Franse netwerk van hedendaagse stripuitgevers gelekt en
    verschenen in het Parijse tijdschrift *Bang!*.

[^9]: Voordat ze erachter kwamen dat de auteur van dit lettertype, dat
    centraal staat in de typografische geschiedenis, zijn dochters
    seksueel misbruikte.

[^10]: Zie ook *The Essential Four Freedoms* zoals geformuleerd door de
    Free Software Foundation: 'De vrijheid om het programma uit te
    voeren zoals jij dat wilt, voor welk doel dan ook (vrijheid 0). De
    vrijheid om te bestuderen hoe het programma werkt en het te
    veranderen zodat het jouw digitale werk doet zoals jij dat wilt
    (vrijheid 1). Toegang tot de broncode is hiervoor een voorwaarde. De
    vrijheid om kopieën te herdistribueren zodat je anderen kunt helpen
    (vrijheid 2). De vrijheid om kopieën van jouw gewijzigde versies te
    distribueren naar anderen (vrijheid 3). Door dit te doen geef je de
    hele gemeenschap een kans om te profiteren van jouw verbeteringen.
    Toegang tot de broncode is hiervoor een voorwaarde.' GNU Project --
    Free Software Foundation. "What Is Free Software?" Geraadpleegd op
    22 november 2023. gnu.org/philosophy/free-sw.en.html#four-free.

[^11]: Open Source Publishing. ['Relearn'](https://osp-kitchen.gitlab.io/ospblog/images/uploads/osp-bat-10-10-1.pdf), △⋔☼, 2011.
    

[^12]: Zie de [fontlog](https://gitlab.constantvzw.org/osp/foundry.crickx/-/blob/master/FONTLOG.txt) die bij elke download zit.
    
[^13]: In 2011 waren de onderzoeksgroepleden Pierre en Ludivine van
    Publi Fluor allebei betrokken bij OSP; Femke had OSP net verlaten om
    samen met Constant de onderzoekseenheid Libre Graphics op te zetten.

[^14]: Paradoxaal genoeg claimde OSP technisch gezien het auteurschap
    van de software van het lettertype, om La Crickx onder een vrije
    licentie te kunnen uitbrengen.

[^15]: Kathy Acker, 'Writing, Identity, and Copyright in the Net Age',
    *The Journal of the Midwest Modern Language Association*, Vol. 28,
    N^o^. 1, Identities (Spring 1995), 93--98. Eigen vertaling.

[^16]: Al snel bleek dat de inkomsten uit het werk rond La Crickx niet
    voldoende waren om de tijd te dekken die nodig was om het
    onderzoeksproject af te ronden op het niveau dat de leden van de
    groep zich hadden voorgesteld. Halverwege werd de beslissing om door
    te gaan genomen doordat elk lid van de groep bevestigend antwoordde
    dat ze 'er niet wakker van zouden liggen \[om gratis te werken\]'.

[^17]: Interview met Chrystel Crickx, november 2021.

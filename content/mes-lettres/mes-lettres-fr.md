Title: Mes Lettres
Date: 2024-12-03 10:20
Authors: Femke Snelting, Pierre Huyghebaert
Summary: résumé
Slug: mes-lettres
Lang: fr


Les lettres volatiles en vinyle que Chrystel Crickx a découpées dans son
arrière-boutique à Schaerbeek sont saturées de gestes intimes. Ces
gestes comprennent l'interaction d'un rasoir extra-fin Le Coq avec la
résistance glissante du vinyle adhésif, le maniement virtuose de sa lame
et d'une paire de ciseaux, le goût pour certaines formes et pas pour
d'autres, les particularités de son corps qui les produit, et l'habitude
relâchée d'optimiser la matière et le temps dans des conditions
matérielles spécifiques. « Mes lettres », comme Chrystel les appelle
simplement, fonctionnent dans un croisement particulier de biographie,
de contexte local et d'époque, et présentent un résultat profondément
personnel. Mais comme le montre cette publication, outre cette échelle
personnelle, les lettres vendues à Publi Fluor traversent bien d'autres
couches de production et de circulation. Si certaines de ces couches
opèrent à une échelle plus locale, d'autres sont de nature industrielle
ou infrastructurelle. Elles sont tour à tour les lettres en vinyle
elles-mêmes, en tant qu'objets à valeur économique dans son magasin et
plus tard en tant qu'archives culturelles, un ensemble de modèles à
ajuster et à redessiner, une histoire matérielle du vinyle, une
collection d'objets vectoriels numérisés touchés par une esthétique
logicielle spécifique, des variations d'une police publiée sous le nom
de « Crickx » sous une licence ouverte, ou des lettres placées par ses
client·es et mises en page par des utilisateur·ices contemporain·es.
L'ensemble de ces couches constitue un écosystème d'objets
intermédiaires physiques et numériques destinés à être posés, placés et
composés par d'autres. Cette texture épaisse d'utilisation et de
réutilisation, d'appropriation et de circulation, soulève de nombreuses
questions sur ce que dire « mes lettres » signifie. À travers une
exploration de multiples histoires des origines (*origin stories*), ce
texte porte sur les façons dont la propriété et la paternité se
mélangent en relation avec ce que nous appelons parfois « la Crickx »
ou, ailleurs, « l'écosystème Publi Fluor ».

### Gestes et patrons

Les notions d'original, de reproduction et donc d'appartenance, d'auteur
et d'attribution fonctionnent différemment selon chaque élément
spécifique de la Crickx. Pour commencer cette exploration, nous
aimerions partir de l'extraordinaire écosystème mis en place par
Publi Fluor, qui, comme l'explique Olivier Bertrand dans
« Verso⥀Recto », s'articule autour de plusieurs entités de lettres qui
se relient par une succession de relations de reproduction. Ces entités
permettent de transférer des formes du positif au négatif, et du patron
aux lettres individuelles dans des couleurs et des tailles multiples.

Pour nous, les patrons font partie des objets les plus fascinants de
l'écosystème Publi Fluor. Ce sont des formes temporairement stables qui
servent de référence matérielle à un stylo à bille pour en tracer le
contour au dos du vinyle. 

![Emboitement manuel des lettres et chiffres.]({static}/images/my-letters/1fr-still-video-bic9.jpg "Emboitement manuel des lettres et chiffres.")

Bien que chaque lettre en vinyle soit
habilement découpée à la main, elle n'est pas entièrement unique car
elle suit le tracé d'un même patron, qui est lui-même une reproduction
d'autres lettres, donc une forme combinatoire. Comme les formes des
lettres ont été développées de manière itérative par Chrystel et son
père, il n'est pas évident de savoir si les patrons atténuent ou
confirment sa paternité, s'ils cristallisent ou diffractent sa touche
personnelle. À travers les gestes répétitifs de traçage et de découpage
de la même forme encore et encore, elle appose sa marque sur chacune des
lettres et dans une certaine mesure les libèrent ainsi d'une signature
trop accentuée.

Le terme utilisé par Chrystel pour désigner ces patrons est gabarit.
Historiquement utilisé dans le contexte de la construction navale, ce
terme désigne les supports rigides demi-ronds autour desquels les
planches sont pliées à la vapeur pour former une coque. Plus
généralement, un gabarit est un objet dont les propriétés physiques
permettent de résister aux forces nécessaires pour façonner telle ou
telle occurrence des objets résultants à des dimensions précises, qu'il
s'agisse de bois, de métal, de tôle ou de tout autre matériau. Dans le
domaine graphique, le terme désigne souvent un négatif, découpé dans une
feuille de matériau rigide et de préférence transparent. Les trous de
cette version courante d'un gabarit peuvent être utilisés pour guider
un couteau, un stylo mécanique ou une table traçante. Les gabarits de
Publi Fluor consistent en des formes *positives* et au lieu de naviguer
à l'intérieur, le stylo trace l'extérieur de la forme sur le vinyle,
laissant une marque qui est ensuite suivie par les ciseaux ou la lame.

De même que chaque pratique induit son propre mode de reproduction, et
produit son propre système de masterisation, son propre objet standard,
on pourrait considérer ces gabarits comme des « originaux » ou des
« sources ». Mais à la différence des poinçons métalliques utilisés pour
créer des caractères désormais classiques, les gabarits de Publi Fluor
ne sont pas conservés dans les réserves précieuses d'une fonderie
prestigieuse ou d'un musée national. Ils n'ont ni leur poids ni leur
valeur, parce que bien qu'ils soient découpés dans un matériau
différent, ils sont presque identiques aux lettres reproduites
elles-mêmes. Les modèles peuvent même être reproduits en utilisant les
lettres en vinyle elles-mêmes, si la bonne taille n'est pas facile à
trouver. Ils sont peut-être aussi remplaçables qu'un dessin vectoriel en
*template* copié et collé à chaque besoin.

![Placement inventif.]({static}/images/my-letters/2fr-pvc.jpg "Placement inventif.")

Après la vente et le transfert littéral de la propriété des lettres, le
travail de lettrage lui-même, c'est-à-dire le placement des lettres dans
une séquence, doit être exécuté par d'autres, selon des routines qui ne
relèvent pas de la pratique de Chrystel. Le ressenti de m·paternité
devient plus flou, comme dans tout transfert typographique, à mesure que
les lettres commencent à fonctionner dans leur propre contexte. Mais
ici, dans l'écosystème Publi Fluor, il y a un espace supplémentaire pour
le jeu, pour la vibration, de l'ajustement des espaces entre les lettres
à leur alignement horizontal et même leur rotation involontaire, un
processus que Chrystel ne peut ou ne veut pas contrôler. Cette relation
détendue et désinhibée entre l'original et la reproduction semble être
au cœur de la pratique de Chrystel, et par conséquent dans la façon dont
ces lettres se retrouvent sur les vitrines, les voitures et les
enseignes. Cela peut nous rappeler la façon dont la combinaison de
l'absence de perte et de l'abondance des copies numériques nourrit la
culture libre et le logiciel libre. Dans cette approche, l'objet
numérique n'est pas rare et ne veut pas être possédé ; et au contraire,
tenter de contrôler ou de restreindre sa reproduction est considéré
comme contre-productif.


### Appartenance autocollante

« Ah oui, c'est mes lettres », remarque Chrystel lorsque nous lui
montrons quelques-unes des façons dont la version numérisée de la
Crickx, la fonte, a été utilisée sur différentes surfaces et dans
différents contextes[^1]. Mais à qui appartiennent vraiment ces lettres
numérisées ? Parce qu'il y a de nombreux points de départ lorsque nous
essayons d'attribuer les versions numériques de la Crickx.

Nous pourrions commencer à la fin des années 90, lorsque, étudiants à
l'erg, Pierre, Karl Bassil et Misch Dimmer commencent à développer une
pratique typographique expérimentale avec un amour pour les formes
vernaculaires sous le nom de Hammerfonts[^2]. Le trio développe des
caractères qui sont souvent des readymades, comme l'Alfphabet, une
police glanée dans le paysage autoroutier belge avant d'en découvrir les
dessins officiels quelques années plus tard[^3]. Le trio rêve de faire
un *roadtrip* qui documenterait et archiverait la *typodiversité* en
voie de disparition sur les enseignes et vitrines des magasins et sur
les panneaux routiers publics. Le projet est de produire des polices
numériques instantanées pendant les heures de conduite et les arrêts sur
les places des petites villes. Mais comment réfléchir l'auctorialité des
traces qu'ils entendaient préserver ? Qui a le privilège de documenter
le vernaculaire, de le nommer, de l'expliciter, et d'en faire une police
de caractères ? L'attribution des formes de lettres produites par des
lettreur·euses largement anonymes ou par des peintres en lettres que
nous ne connaissons pas, pourrait un peu facilement être transférée à
ces archivistes, surtout lorsque des versions numérisées commenceront
à circuler après avoir été soigneusement photographiées, vectorisées et
interlettrées. Même si le safari typo rêvé à l'échelle XXL n'a
finalement jamais eu lieu, on peut imaginer comment la collecte et la
réutilisation ultérieure de la pratique disparue du lettrage est à la
fois un acte de soin et un acte de capture.

On pourrait aussi commencer quand, ayant remarqué la Crickx dans les
rues de Schaerbeek, Pierre retrace la présence des lettres jusqu'à
trouver le magasin Publi Fluor et en achète un petit nombre, en
sélectionnant une taille moyenne facile à scanner tout en gardant
suffisamment de détails[^4]. Les lettres scannées et vectorisées ne sont
pas encore une police de caractères, mais plutôt un dossier d'objets
numériques incomplets qui doivent être dupliqués, alignés et espacés
manuellement dans un logiciel vectoriel afin de composer un texte.
Travailler avec des proto-fontes aussi rustiques est une pratique
numérique courante chez les graphistes qui s'essaient à la typographie.
Les lettres fonctionnant toujours comme des objets distincts, cette
pratique reste proche de la manière dont les client·es de Publi Fluor
finissent par placer manuellement des lettres individuelles sur leurs
vitrines. Acheter un jeu de lettres signifie-t-il aussi avoir le droit
de les reproduire numériquement ? D'un point de vue juridique, c'est
probablement le cas. Le cadre du droit d'auteur pour les caractères
numériques repose sur l'hypothèse qu'ils sont « trop utiles pour être
protégés ». Comme l'explique Eric Schrijver dans *Copiez ce livre*,
« leur lien traditionnel avec la technologie ne laissait pas supposer
que des protections s'appliqueraient aux caractères typographiques de la
même manière qu'aux beaux-arts et à l'écriture »[^5]. Le droit
d'auteur·ice fonctionne plus ou moins de la même manière pour une police
de caractères que pour le code source d'un logiciel, ce qui signifie que
leurs conceptions ou expressions spécifiques ne sont pas protégées, mais
seulement leurs encodages en tant que police de caractères. Légalement,
tout le monde a donc le droit de numériser ces lettres, mais sur le plan
éthique, la situation est peut-être un peu différente. Pierre ne cache
pas à Chrystel qu'il va scanner les lettres achetées, mais sans pratique
numérique, elle n'en a peut-être pas compris les conséquences[^6]. Elle
ne refuse pas non plus de répondre aux questions de Pierre, sachant
qu'il écrit un article sur son travail. Mais, malgré le plaisir évident
qu'elle éprouve à faire l'objet d'une publication, est-elle tout à fait
consciente que cela la rendra plus identifiable, avec une potentielle
atteinte à sa vie privée, importante pour elle par ailleurs[^7] ?

Pour rendre possible une composition de texte logicielle, Pierre
complète les tracés vectoriels et les insère dans une police de
caractères. Il nomme temporairement la fonte résultante « Crickx Rush »
pour marquer le fait qu'il reste manifestement du travail à faire, mais
aussi peut-être pour souligner son état d'esprit face au flot continu de
projets permettant mal de sortir d'une relative précarité économique.
Dans cette version, la vectorisation automatique collabore
singulièrement avec les arêtes vives des lettres découpées à la main
pour former des angles, des coudes et des cuspides typiques à la fois de
ses algorithmes et des découpes manuelles. Il choisit de placer les
lettres plus ou moins sur la même ligne de base et ne passe pas trop de
temps à ajuster l'interlettrage. Le Crickx Rush mélange différents
styles, car le jeu que Pierre a acheté à l'origine comprend plusieurs
modifications itératives entre des lettres qui ont probablement été
découpées par le père de Chrystel, et celles qui ont été découpées par
Chrystel elle-même. Cet objet numérique hybride circule sous le nom de
femme mariée de Chrystel Crickx, mais est parfois attribué de manière
informelle à Pierre ou à Hammerfonts[^8].

Un autre fil conducteur déplace plus loin la question de la propriété,
de la paternité et de l'attribution. Les lettres minuscules étant
absentes du stock de Chrystel, Pierre rend visite à la lettreuse
désormais à la retraite dans sa maison de la campagne wallonne et lui
commande un ensemble de glyphes minuscules accompagné des lettres
accentuées et des symboles courants. Il craint que la police Gill
Sans[^9], qu'il utilise pour communiquer le dessin de ces signes dont il
a besoin, et dont certains sont inconnus pour Chrystel, ne l'influence
dans ses choix de conception. Mais cette crainte s'avère inutile car
lorsqu'il reçoit les lettres découpées en vinyle quelques semaines plus
tard, la lettreuse a emprunté une voie surprenante. L'alphabet que nous
appelons désormais la « Chrystelise » est incroyablement autonome,
imaginatif et psychédélique, mais son design n'a aucun rapport avec les
capitales de la Crickx. Il s'agit d'une anomalie intéressante dans
l'ensemble et qui, de par sa conception en tant qu'ensemble cohérent,
pourrait être considéré comme plus proche de la pratique de la
typographie. Cet alignement sur des pratiques de conception plus
conventionnelles signifie-t-il que la Chrystelise peut être attribuée de
manière moins ambiguë à Chrystel ?

Lorsque Chrystel prend sa retraite, Pierre achète le stock complet de
Publi Fluor, y compris certains de ses meubles de rangement, ses modèles
et une partie des archives administratives pour la somme de 10 000
francs belges (environ 1 500 euros de nos jours). Plus tard, il
rencontre Ludi Loiseau, travaille avec elle chez Speculoos, Antoine
Begon les rejoint, et, en parallèle, à peu de temps de différence,
chacun·e rallie Open Source Publishing (OSP). La proximité avec ce récit
et l'archive amène Ludi et Antoine à redigitaliser les lettres en vinyle
avec un peu moins de précipitation, en incluant également les minuscules
autonomes de la Chrystelise. Ensemble, i·els reconsidèrent et
redessinent la trace vectorielle de chaque lettre, et font des choix
typographiques là où c'est nécessaire.

Lorsque la fonte, qui comprend maintenant plusieurs versions de la
Crickx, est prête à être rééditée, la publier sous une Open Font Licence
(OFL) pose peu de questions. Il semble en effet que la Crickx, devenue
un objet encore plus hybride, gagnerait à circuler dans les conditions
généreuses qu'offre cette licence. L'OFL fait partie d'un genre
d'interventions paralégales que l'on pourrait regrouper sous les
licences de contenu ouvert, de copyleft ou de culture libre. Ces
licences contournent le droit d'auteur conventionnel pour rendre
possibles d'autres modes de partage dans le cadre de la loi en
permettant explicitement aux utilisateur·ices d'étudier, d'améliorer, de
distribuer et de copier la police de caractères[^10]. Peut-être plus
important encore, les licences de culture libre incitent à une
ré-imagination urgente de la m·paternité en tant que pratique
relationnelle en réseau, ou comme le déclare OSP en 2011, « notre
enthousiasme pour le logiciel libre vient de sa conception même
puisqu'il est basé sur une pratique collective qui crée un réseau de
relations entre des communautés, des outils et des pratiques
spécifiques »[^11].

![Revendication de la m·paternité de la version numérisée de la Crickx dans son fontlog.]({static}/images/my-letters/3fr-fontlog-20240222_220219.jpg "Revendication de la m·paternité de la version numérisée de la Crickx dans son fontlog.")

En lisant dix ans plus tard les crédits stockés dans le *fontlog*[^12]
(journal de la police) qui accompagne les fontes, il est surprenant de
trouver la mention sans équivoque « Copyright (C) 2011 OSP ».
L'insertion d'OSP dans la généalogie des auteurices de ces lettres et la
revendication de la m·paternité de la version numérisée de la Crickx
n'ont pas suscité beaucoup d'intérêt à l'époque, mais soulèvent
aujourd'hui de nombreuses questions[^13]. Pourquoi OSP s'est-il senti
autorisé à cette publication et à diffuser le tout sous cette licence
spécifique[^14] ? Et si nous problématisons déjà cette autorisation
individualisée, comment rendre compte du rôle du vinyle, du rasoir, de
l'algorithme qui a défini ses contours numériques, de l'esthétique
bruxelloise qui a co-défini ses formes et de l'enthousiasme continu des
utilisateur·ices contemporain·es ? Comment leurs interprétations
affectent-elles la police et co-construisent-elles sa réutilisation
future ? Le collectif avait-il raison de supposer que la Crickx était
déjà, d'une certaine manière, une ressource partagée, ou a-t-il ainsi
revendiqué la m·paternité de Chrystel en s'affirmant lui-même ?
Continuer à appeler cette police de caractères « Crickx » était une
façon de glisser une artisane locale et ses lettres situées dans une
liste de typographes renommés quelque part après John Baskerville et
avant Adrian Frutiger. Mais c'était aussi établir Chrystel comme une
autrice parmi eulles, en qualifiant à tort de typo­graphie la pratique
précise et intentionnelle du lettrage de Publi Fluor. 

Plus tard, quand OSP déménage avec Constant dans l'espace de travail
Variable, lié aux pratiques artistiques libres, gratuites, open source
et situé à quelques rues de la boutique Publi Fluor, le stock de lettres
retourne également à Schaerbeek. Les lettres de couleurs vives sont
rapidement adoptées par tous les membres de la communauté, et Chrystel
accepte l'invitation d'assister à l'ouverture du lieu et à la réédition
de la Crickx en tant qu'invitée d'honneur. Si leur association avec les
fontes et l'histoire de Chrystel augmente le capital culturel d'OSP, on
ne voit pas très bien ce qu'elle apporte à Chrystel, si ce n'est un
nouveau double mouvement de soin et de capture.

Ce mélange, cette reconnaissance mutuelle et cette validation qui se
sont produites lors de la numérisation de l'écosystème Publi Fluor ont
mis un certain temps à devenir lisibles. La numérisation de la Crickx
est une tentative toujours en cours de libérer les lettres de leurs
archives physiques, de leurs dépendances à l'hyperlocal, et a peut-être
empêché sa disparition. La circulation célèbre cette relation entre les
différents types d'usage et de travail, et la culture du Libre nous
fournit un cadre pour le faire. Mais pour contrecarrer la sédimentation
dans des relations conventionnelles avec l'autorat, une licence copyleft
n'est pas suffisante. Nous devons continuer à nous interroger sur les
conditions d'utilisation et de réutilisation, sur ce qui est reconnu
comme original et ce qui est reconnu comme copie, sur ce qui est
toujours mis en avant et ce qui est toujours laissé à l'arrière-plan.

### Gagner sa vie

Dans un texte écrit en 1996, l'auteure féministe Kathy Acker évoque la
relation complexe qu'elle entretient, en tant que marxiste, avec le
droit d'auteur : « En tant qu'écrivains, nous sommes économiquement
dépendants du droit d'auteur, de son existence, parce que nous vivons et
travaillons, que nous le voulions ou non, dans une société industrielle
bourgeoise, dans une société capitaliste, une société basée sur la
propriété. Nous avons besoin de posséder pour survivre, en fait, pour
être »[^15]. Dans le contexte d'une économie de marché, l'expression
intellectuelle d'un auteur n'est qu'un objet qui peut être possédé comme
n'importe quel autre, une marchandise qui peut être échangée et
exploitée. Pour Acker, les transactions qui lui permettent de payer son
loyer étaient basées sur des textes profondément autobiographiques qu'on
lui a commandé d'écrire ou pour lesquels elle était payée par des droits
d'auteur et des contrats d'édition. Vivre de son travail créatif dans un
système capitaliste produit des dépendances paradoxales, où survivre et
être commencent à se confondre d'une manière bien trop familière.

Pour les professionnel·les de la culture libre, ces paradoxes se
présentent différemment, mais ils ne disparaissent pas pour autant. La
pratique du Libre et de l'Open Source repose sur l'hypothèse que les
objets numériques existent sans perte, ce qui signifie que les copies ne
sont pas différentes de leurs soi-disant originaux. Par essence, un
fichier numérique ne peut pas et ne doit donc pas faire l'objet d'une
propriété exclusive. Cela signifie que si vous voulez gagner votre vie
avec la culture libre, vous devez trouver un moyen de facturer des
services, des applications spéciales ou du développement personnalisé
plutôt que de vendre et capitaliser sur des objets. Dans l'environnement
néo-libéral de la production culturelle, cela coïncide avec l'importance
croissante du capital culturel et dans le même temps de la valeur de
l'autorat reconnaissable. La gestion d'une signature ou d'une identité
de marque dans cette promiscuité remplace une économie basée sur la
propriété en tant que bien. 

À l'échelle à laquelle la plupart d'entre nous travaillons, le
« business model» de la typographie numérique commerciale n'a pas
vraiment de sens. Les polices de caractères numériques doivent être
copiées pour fonctionner, et la rareté artificielle est impossible à
atteindre pour qui que ce soit d'autre que les grandes fonderies de
caractères qui combinent une armée d'avocat·es avec une culture de la
peur. La décision d'accorder une licence à la Crickx dans des
conditions qui permettent à d'autres de l'utiliser librement n'a donc
pas seulement une signification politique et de bonnes intentions, mais
suit d'une certaine manière également une logique commerciale. Les
projets autour de la Crickx n'ont rendu aucun·e d'entre nous (OSP, les
utilisatrickx, les chercheur·euses) riche[^16], mais notre engagement en
faveur de sa pérennité en tant que police numérique a généré du capital
culturel qui a mené à plusieurs invitations pour des ateliers, des
présentations et enfin ce projet de recherche financé par deux
subventions culturelles.

![Le stock de lettres dans son installation à Variable.]({static}/images/my-letters/4fr-PA144171.JPG "Le stock de lettres dans son installation à Variable.")

Alors que les fontes Crickx circulent dans les conditions généreuses de
l'OFL, les objets physiques continuent d'enthousiasmer les
confrère·sœurs, les étudiant·es et d'autres personnes. Variable, OSP, et
plus tard Pierre et Sophie Boiron lorsque Variable ferme ses portes et
que les archives retournent chez Spec uloos, assument un rôle de
gardien·nes des archives. Après la retraite de Chrystel, la Crickx est
devenue plus pleinement un objet culturel, ce qui a également modifié le
statut du stock de lettres. Depuis le rachat, les lettres ne sont plus à
vendre, le stock est passé d'un produit commercial à une curiosité
culturelle, voire à un m·patrimoine. Même si le stock s'épuise
lentement, car aucune nouvelle lettre n'est découpée, il semble
important que les lettres physiques continuent à s'afficher dans les
rues de Bruxelles. Alors Pierre et Sophie pratiquent une générosité
stratégique, donnant avec parcimonie les lettres en vinyle à certaines
personnes et pas à d'autres. Sans surprise, cette approche a choqué
Chrystel lorsque nous lui en avons parlé. Comment ces objets, qui
représentaient son gagne-pain, pouvaient-ils être donnés gratuitement ?

Il est curieux de penser aux nombreux contrastes entre la pratique de
Publi Fluor et la nôtre. Par exemple, les client·es de Publi Fluor ne
semblent pas gêné·es par le fait que leur signalétique porte la
signature reconnaissable de Chrystel, en résonance avec le paysage
urbain bruxellois et non avec leur propre marque. Mais apprécient-i·els
réellement son style et son expertise, ou préféreraient-i·els un
lettrage découpé à la machine s'ils en avaient les moyens ? En
parcourant l'administration manuscrite de Chrystel, écrite au dos des
devis et autres lettres commerciales[^17], nous essayons d'imaginer le
fonctionnement quotidien de Publi Fluor et comment, en tant que
commerçante, elle décidait de la quantité de travail qu'elle pouvait
accepter, du stock qu'elle devait préparer et du nombre de lettres
qu'elle devait vendre par jour, par semaine ou au cours d'une vie, afin
d'établir une situation économique suffisamment confortable pour elle et
pour sa famille. Il se pourrait que ces gestes économiques, qui
résultent du pragmatisme multi-scalaire qu'implique la création d'une
vie à partir de lettres découpées, définissent la qualité de
l'écosystème Crickx. Optimisé pour un minimum de spéculation et
d'ambition, le fait d'être juste assez, juste assez bon, a un impact sur
leur esthétique et certainement sur l'attrait qu'elles exercent sur
nous.

En disant « C'est mes lettres », Chrystel exprime un sentiment de
propriété qui ne semble pas être une revendication d'auteur·ice ou
artistique. Le « mes » est plutôt propriétaire dans le sens de « fait
par moi, à mon époque ». L'utilisation du singulier « c'est » est sans
doute du français bruxellois familier, mais peut aussi désigner, au
singulier, une pratique plutôt que des objets individuels. « C'est mes
lettres » établit une relation directe entre son travail personnel, sa
vie et des objets reconnaissables dans le monde.


### Un engagement à partager

La recherche Publi Fluor a été partiellement motivée par un malaise
quant à la manière dont la dénomination, l'octroi de licences et la
contextualisation de la police numérique en tant que culture libre
semblaient perdre de vue les conditions collectives qui ont rendu et
continuent de rendre possible l'écosystème Publi Fluor. Dans notre
pratique quotidienne, nous essayons de résister au cadre idéologique
dominant du droit conventionnel de la propriété intellectuelle et à ses
hypothèses standardisées sur la m·paternité individuelle, l'exclusivité
et l'originalité. Cela coïncide avec un intérêt profond pour la pratique
du logiciel libre, de l'Open Source et de la culture libre, qui ont
énergisé notre travail pendant de nombreuses années. Dans notre
engagement avec l'écosystème Publi Fluor, nous avons donc dû nous
demander comment aller au-delà de la sédimentation des biographies
individuelles et des adaptations impliquées dans ce geste, en
particulier parce que la technique juridique de la licence commence par
l'établissement d'un auteur légal. Comment plutôt traiter ce système
comme un écosystème de production déjà collectif, à utiliser, enrichir
et remettre en circulation ? L'auteure mexicaine Cristina Riviera Garza
propose la *désappropriation* comme pratique créative pour résister au
système capitaliste qui définit le travail et la vie. En tant que
pratique de capture avec soin, la désappropriation est un mode collectif
d'écriture, de fabrication et de création avec le travail des autres,
une manière d'exposer « la pluralité qui précède l'individualité dans le
processus créatif, ouvrant une fenêtre sur la stratification matérielle
si souvent dissimulée par les textes d'appropriation »[^18]. Il semble
que les différentes constellations de personnes restées liées à ces
volatiles lettres de vinyle pendant plus de deux décennies ont fait un
geste de désappropriation du même ordre, en s'engageant avant tout dans
des moyens d'articuler et de partager les pluralités qui constituent la
pratique de Publi Fluor.

Cette publication s'accompagne d'une quatrième ou cinquième réédition
des polices numériques que nous appelions auparavant la Crickx. Les
formes de lettres ont été mises à jour et adaptées, et comprennent
maintenant un ensemble de nouveaux glyphes qui donnent une forme
typographique au besoin urgent d'un langage inclusif du point de vue du
genre. Nous avons réécrit le *fontlog*, en réponse à certaines des
questions explorées dans ce texte. Nous avons également renommé la
collection de polices en « Publi Fluor », pour tenter de passer de la
focalisation sur une seule personne à une activité, une pratique. Ce
changement de nom risque bien sûr d'être interprété comme une sorte de
déshommage, d'effacement, car il est important de savoir qui compose,
écrit, codifie, dessine ou découpe. Mais comme l'écrit Sara Ahmed, il
existe une tension entre la reconnaissance et l'individualisation, car
« une approche féministe ne peut se permettre de réduire les questions
d'incarnation et de subjectivité à l'ontologie de l'identité », en
d'autres termes, nous ne pouvons pas supposer que l'incarnation de
l'auteur·ice est parfaitement alignée avec son identité, ni avec l'œuvre
elle-même[^19]. Nous vous invitons donc à imaginer ce livre comme un
spécimen typographique étendu, qui, en plus de présenter les qualités
esthétiques extraordinaires d'une pratique locale du lettrage, insiste
sur le contexte socio-économique de l'écosystème Publi Fluor, plutôt que
d'essayer de célébrer le génie d'une seule inventrice. L'expérience qui
consiste à accepter la spécificité de la pratique de Chrystel tout en
s'appropriant ses lettres, documentée dans ce livre, traverse de
nombreuses compréhensions différentes de ce que signifie dire « mes
lettres ». Elle nous permet de repenser cette expression dans des
directions moins figées que celles que les pratiques typographiques,
graphiques, logicielles et industrielles conventionnelles autorisent
habituellement.

[^1]: Entretien avec Chrystel Crickx, novembre 2021.

[^2]: Pour mieux comprendre les chevauchements entre Hammerfonts,
    Constant, Variable, OSP et Spec uloos, voir l'« Écosystème Publi
    Fluor ».

[^3]: Open Source Publishing. « Alphabet » OSP Foundry, septembre 2013.
    osp.kitchen/foundry/alfphabet

[^4]: Ou remonter de manière plus détaillée au milieu des années 1980,
    quand Pierre voit ces lettres sur les vitrines de Schaerbeek lors de
    ses premières escapades adolescentes en solitaire à la capitale. Ou
    encore aux milieu des années 1990, quand Pierre travaille avec
    Vincent Fortemps sur des projets pour les Halles de Schaerbeek,
    parle avec lui de ces lettres et trouve la boutique qui les produit.

[^5]:  Eric Schrijver & co., *Copiez ce livre -- Un manuel sur le droit
    d'auteur et les communs culturels, par et pour les artistes*, Cluny,
    Les Commissaires Anonymes, 2018, p.  133.

[^6]: Pierre rapporte une discussion avec Chrystel au moment de lui
    présenter la première affiche où ses lettres apparaissent. « Comment
    est lettrée l'affiche, avec trois fois la lettre N alors que tu
    (Pierre) n'en as acheté qu'une ? ». Et comment les tailles de lettre
    de l'affiche avaient été produites, sans rapport avec les tailles de
    lettres vendues par le magasin. Les tentatives d'explications de
    vulgarisation technique de Pierre se sont vite heurtées à une
    certaine résignation de Chrystel. « De toute façon, je ne sais même
    pas utiliser un fax... »

[^7]: Lors des différents échanges au cours de la rédaction de ce livre,
    ce souhait de discrétion est revenu plusieurs fois, avec une demande
    explicite de la part de Chrystel de ne pas révéler d'élément
    permettant de situer l'adresse actuelle de son domicile.

[^8]: Vers 2003, la police crickx-rush-light-ext, l'une des nombreuses
    versions optimisées produites par Speculoos pour être utilisées dans
    des sites web en Flash, attribuée à Hammerfonts, fait l'objet d'une
    fuite à travers le réseau belgo-français des éditeurs de bande
    dessinée contemporaine et apparaît dans le magazine de BD parisien
    *Bang !*.

[^9]: Avant d'apprendre que l'auteur de cette police de caractères,
    centrale dans l'histoire de la typographie, abusait sexuellement de
    ses filles.

[^10]: Voir aussi : *The Essential Four Freedoms* tel que formulé par la
    Free Software Foundation : « La liberté d'exécuter le programme
    comme vous le souhaitez, dans n'importe quel but (liberté 0) ; La
    liberté d'étudier le fonctionnement du programme, et de le modifier
    pour qu'il fasse vos calculs comme vous le souhaitez (liberté 1) ;
    L'accès au code source est une condition préalable à cela  ; La
    liberté de redistribuer des copies pour que vous puissiez aider les
    autres (liberté 2) ; La liberté de distribuer des copies de vos
    versions modifiées à d'autres (liberté 3). « En faisant cela, vous
    pouvez donner à l'ensemble de la communauté une chance de bénéficier
    de vos modifications. L'accès au code source est une condition
    préalable à cela ». gnu.org/philosophy/free-sw.en.html#four-free

[^11]: Open Source Publishing. [« Relearn »](https://osp-kitchen.gitlab.io/ospblog/images/uploads/osp-bat-10-10-1.pdf), △⋔☼, 2011.
    

[^12]: Voir le [*fontlog*](https://gitlab.constantvzw.org/osp/foundry.crickx/-/blob/master/FONTLOG.txt) inclus dans chaque téléchargement :
    

[^13]: En 2011, Pierre et Ludivine, membres du groupe de recherche Publi
    Fluor, étaient tous·tes deux impliqué·es dans OSP ; Femke venait de
    quitter OSP pour initier le Libre Graphics Research Unit avec
    Constant.

[^14]: Paradoxalement, OSP a techniquement revendiqué la paternité du
    logiciel de la police, afin de libérer la fonte.

[^15]: Kathy Acker, « Writing, Identity, and Copyright in the Net Age »,
    *The Journal of the Midwest Modern Language Association*, Vol. 28,
    No 1, Identities (printemps 1995), pp. 93--98.

[^16]: Il s'est avéré que les revenus générés par le travail autour de
    La Crickx ne permettaient pas de couvrir le temps nécessaire pour
    mener à bien le projet de recherche au niveau imaginé par les
    membres du groupe. À mi-parcours, la décision de continuer a été
    prise par chaque membre du groupe qui a répondu à la question
    suivante : « Je ne perdrai pas le sommeil à cause de cela
    [travailler gratuitement]. »

[^17]: Entretien avec Chrystel Crickx, novembre 2021.

[^18]: Cristina Rivera Garza*, The Restless Dead : Necrowriting and
    Disappropriation*, Nashville, Vanderbilt University Press, 2020, p.
    65.

[^19]: Sara Ahmed, *Differences That Matter: Feminist Theory and
    Postmodernism*, Cambridge: Cambridge University Press, 1998, p. 123.

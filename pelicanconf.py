AUTHOR = 'OSP, Surfaces utiles'
SITENAME = 'Publifluor'

PATH = 'content'

TIMEZONE = 'Europe/Paris'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (('Pelican', 'https://getpelican.com/'),
         ('Python.org', 'https://www.python.org/'),
         ('Jinja2', 'https://palletsprojects.com/p/jinja/'),
         ('You can modify those links in your config file', '#'),)

# Social widget
SOCIAL = (('You can add links in your config file', '#'),
          ('Another social link', '#'),)

DEFAULT_PAGINATION = False

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True

# disable the generation of those default html files
AUTHOR_URL = AUTHOR_SAVE_AS = ''
TAG_URL = TAG_SAVE_AS = ''
CATEGORY_URL = CATEGORY_SAVE_AS = ''

DIRECT_TEMPLATES = ['index', 'specimen', 'textes']

THEME = 'theme'

DEFAULT_LANG = 'fr'
MAIN_LANG = 'fr'

PLUGIN_PATHS = ['plugins']
PLUGINS = ['i18n']

SITEURL = ''

I18N_SUBSITES = {
    'nl':{
        'SITENAME': 'Publifluortje', 
        'SITEURL': '/nl'
    },
    'en':{
        'SITENAME': 'Publifluormax',
        'SITEURL': '/en'

    },
}

